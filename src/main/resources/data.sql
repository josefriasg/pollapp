 INSERT INTO USUARIO VALUES ((SELECT nextval('hibernate_sequence')), 'joseduardofrias@gmail.com', false, true, 'PollApp', '$2a$11$CDeyA.07oteeb3dj7OlxS.7EIoBr0ACY1tNfCe6pkkLSuGOHMtu8G', 1);
 
 
CREATE OR REPLACE FUNCTION updatePredictions() RETURNS trigger  AS '
DECLARE
avg1 NUMERIC(8,2) := 0;
avg2 NUMERIC(8,2) := 0;
stddev1 NUMERIC(8,2) := 0;
stddev2 NUMERIC(8,2) := 0;
result1 INTEGER := 0;
result2 INTEGER := 0;
predId INTEGER := 0;
existingPred INTEGER := 0;
predicUser CURSOR(partidoParam integer) FOR SELECT pu.id_predicciones_usuario AS idPredUsuario FROM predicciones_usuario pu, polla po, torneo tor, partido pa WHERE pu.usuario_user_id=1 AND pa.id_partido = partidoParam AND pa.torneo_id_torneo = tor.id_torneo AND tor.id_torneo = po.torneo_id_torneo AND pu.polla_id_polla = po.id_polla;
predRecord RECORD;
BEGIN
	IF OLD.resultado_final_equipo1=-1 OR OLD.resultado_final_equipo2=-1 THEN
		SELECT AVG(resultado_equipo1), STDDEV(resultado_equipo1), AVG(resultado_equipo2), STDDEV(resultado_equipo2) into avg1, stddev1 ,  avg2, stddev2 FROM prediccion_partido WHERE partido_id_partido = OLD.id_partido;
		
		IF stddev1 IS NULL OR stddev1=0 THEN
			result1 = avg1;
		ELSE
			SELECT AVG(resultado_equipo1) INTO result1 FROM prediccion_partido WHERE partido_id_partido = OLD.id_partido AND resultado_equipo1 BETWEEN (avg1-stddev1) AND (avg1+stddev1);
			IF result1 IS NULL THEN
				result1 = avg1;
			END IF;
		END IF;
		
		IF stddev2 IS NULL OR stddev2=0 THEN
			result2 = avg2;
		ELSE
			SELECT AVG(resultado_equipo2) INTO result2 FROM prediccion_partido WHERE partido_id_partido = OLD.id_partido AND resultado_equipo2 BETWEEN (avg2-stddev2) AND (avg2+stddev2);
			IF result2 IS NULL THEN
				result2 = avg2;
			END IF;	
		END IF;
		
		
		IF result1 IS NULL THEN
			result1 = 0;
		END IF;
		
		IF result2 IS NULL THEN
			result2 = 0;
		END IF;	
		
		OPEN predicUser(partidoParam:=OLD.id_partido);
		
		LOOP
			FETCH predicUser INTO predRecord;
    		EXIT WHEN NOT FOUND;
			
			IF NOT EXISTS (SELECT 1 from predicciones_usuario pu LEFT OUTER JOIN predicciones_usuario_prediccion_partido pupp ON (pu.id_predicciones_usuario = pupp.predicciones_usuario_id_predicciones_usuario) LEFT OUTER JOIN prediccion_partido pp on (pupp.prediccion_partido_id_prediccion_partido = pp.id_prediccion_partido) WHERE pp.partido_id_partido = OLD.id_partido and pu.usuario_user_id=1 and pu.id_predicciones_usuario=predRecord.idPredUsuario) THEN
				SELECT nextval('hibernate_sequence') into predId;
				INSERT INTO prediccion_partido VALUES (predId,result1,result2, OLD.id_partido);
				INSERT INTO predicciones_usuario_prediccion_partido VALUES (predRecord.idPredUsuario, predId);
			ELSE
				SELECT pp.id_prediccion_partido INTO existingPred from predicciones_usuario pu LEFT OUTER JOIN predicciones_usuario_prediccion_partido pupp ON (pu.id_predicciones_usuario = pupp.predicciones_usuario_id_predicciones_usuario) LEFT OUTER JOIN prediccion_partido pp on (pupp.prediccion_partido_id_prediccion_partido = pp.id_prediccion_partido) WHERE pp.partido_id_partido = OLD.id_partido and pu.usuario_user_id=1 and pu.id_predicciones_usuario=predRecord.idPredUsuario;
				UPDATE prediccion_partido SET resultado_equipo1 = result1, resultado_equipo2 = result2 WHERE id_prediccion_partido = existingPred;
			END IF;
		END LOOP ; 
		CLOSE predicUser;
	END IF;
	RETURN NEW;
END;
' LANGUAGE plpgsql;

CREATE TRIGGER resultado_changes
  AFTER UPDATE
  ON partido
  FOR EACH ROW
  EXECUTE PROCEDURE updatePredictions();