package com.pollapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PaisDto {

    private Integer idPais;
	
	private String name;
	
	private String externalId;
}
