package com.pollapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PrediccionPartidoDto {

    private Integer idPrediccionPartido;
	
	private PartidoDto partido;
	
	private Integer resultadoEquipo1;
	
	private Integer resultadoEquipo2;
	
}
