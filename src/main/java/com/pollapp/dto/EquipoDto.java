package com.pollapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class EquipoDto {

    private Integer idEquipo;
	
	private String name;

	private PaisDto pais;
	
	private Integer externalId;
	
	private String urlEscudo;
}
