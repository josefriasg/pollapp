package com.pollapp.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PollaDto {

    private Integer idPolla;
	
	private String name;
	
	private TorneoDto torneo;
	
	private UsuarioDto creador;
	
	private String emailsInvitados;
	
	private Integer puntosExactoEquipo;
	
	private Integer puntosGanadorOEmpate;
	
    private Integer puntosDiferencia;
	
	private String uniqueCode;
	
	private List<PrediccionesUsuarioDto> prediccionesUsuario;

}
