package com.pollapp.dto;



import com.pollapp.model.enums.Rol;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class UsuarioDto {
	
    private Integer userId;
	
	private String name;
	
	private String email;
	
	private String password;
	
	private boolean isEnabled;
	
	private boolean isDropped;
	
	private Rol role;
	
	private boolean wantEmails;

}
