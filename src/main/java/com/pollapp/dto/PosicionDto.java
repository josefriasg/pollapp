package com.pollapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PosicionDto implements Comparable<PosicionDto>{
	
	private Integer posicion;
	private String nombre;
	private Integer puntos;
	private Integer userId;
	private byte[] picture;

	@Override
	public int compareTo(PosicionDto o) {
		return this.getPuntos()==o.getPuntos()?this.getNombre().compareTo(o.getNombre()): 
			this.getPuntos().compareTo(o.getPuntos());
	}
}
