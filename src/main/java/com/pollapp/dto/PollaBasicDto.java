package com.pollapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PollaBasicDto {
	private Integer idPolla;
	
	private String name;
	
	private Integer puntosExactoEquipo;
	
	private Integer puntosGanadorOEmpate;
	
    private Integer puntosDiferencia;
	
	private String uniqueCode;
	
}
