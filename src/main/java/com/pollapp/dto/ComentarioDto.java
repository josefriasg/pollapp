package com.pollapp.dto;

import java.util.Date;


import com.pollapp.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ComentarioDto {
	
	private Integer idComentario;
	
	private String comentario;
	
	private Usuario usuario;
	
	private Date curDate;
}
