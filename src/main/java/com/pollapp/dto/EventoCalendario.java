package com.pollapp.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class EventoCalendario {
	
	private String text;
	private Date start;
	private Date end;
	private String color;
	
}
