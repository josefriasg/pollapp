package com.pollapp.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PartidoDto {

    private Integer idPartido;
	
	private EquipoDto equipo1;
	
	private EquipoDto equipo2;

	private TorneoDto torneo;
	
	
	private Integer resultadoFinalEquipo1;
	
	private Integer resultadoFinalEquipo2;
	
	private Date inicio; 
	
	private Boolean esFinal;
	
	private Integer externalId;
}
