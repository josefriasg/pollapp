package com.pollapp.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PrediccionesUsuarioDto {

    private Integer idPrediccionesUsuario;
	
	private UsuarioDto usuario;
	
	private PollaDto polla;
	
	List<PrediccionPartidoDto> prediccionPartido;

}
