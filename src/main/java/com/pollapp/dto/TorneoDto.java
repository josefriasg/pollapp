package com.pollapp.dto;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class TorneoDto {

    private Integer idTorneo;
	
	private String name;
	
	private Set<PartidoDto> partidos;
	
	private Integer externalId;
	
	private boolean isActive;

	
}
