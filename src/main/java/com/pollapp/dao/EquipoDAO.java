package com.pollapp.dao;


import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pollapp.model.Equipo;
import com.pollapp.model.Pais;

public interface EquipoDAO extends CrudRepository<Equipo, Integer>{
	List<Equipo> findByPaisOrderByName(Pais pais);
	Optional<Equipo> findByExternalId(Integer externalId);
}
