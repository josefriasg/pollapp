package com.pollapp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pollapp.model.Polla;
import com.pollapp.model.Torneo;
import com.pollapp.model.Usuario;

public interface PollaDAO extends CrudRepository<Polla, Integer>{
	
	public List<Polla> findByUniqueCode(String uniqueCode);
	
	@Query("SELECT p FROM Polla p, PrediccionesUsuario pred WHERE p = pred.polla AND pred.usuario = :usuario ORDER BY p.idPolla DESC")
	public List<Polla> getPollasByParticipante(@Param("usuario")Usuario usuario);
	
	@Query("SELECT p FROM Polla p JOIN FETCH p.torneo WHERE p.idPolla = :id")
    public List<Polla> findByIdAndFetchTorneoEagerly(@Param("id") Integer id);
	
	public List<Polla> findByTorneoIn(List<Torneo> torneos);

}
