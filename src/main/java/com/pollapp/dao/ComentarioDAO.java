package com.pollapp.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pollapp.model.Comentario;

public interface ComentarioDAO extends CrudRepository<Comentario, Integer>{
	public List<Comentario> findAllByOrderByCurDateDesc();
}
