package com.pollapp.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pollapp.model.Partido;
import com.pollapp.model.Polla;
import com.pollapp.model.PrediccionesUsuario;
import com.pollapp.model.Usuario;


public interface PrediccionesUsuarioDAO extends CrudRepository<PrediccionesUsuario, Integer>{

	public List<PrediccionesUsuario> findByUsuarioAndPolla(Usuario usuario, Polla polla);
	public List<PrediccionesUsuario> findByUsuario(Usuario usuario);
	public List<PrediccionesUsuario> findDistinctByUsuarioAndPrediccionPartidoPartidoInicioBetween(Usuario usuario, Date start, Date end);
	public List<PrediccionesUsuario> findByPolla(Polla polla);
	
}
