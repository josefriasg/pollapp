package com.pollapp.dao;


import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pollapp.model.Usuario;

public interface UsuarioDAO extends CrudRepository<Usuario, Integer>{

	Optional<Usuario> findByEmail(String email);
}
