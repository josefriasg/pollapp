package com.pollapp.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pollapp.model.Partido;
import com.pollapp.model.Torneo;


public interface PartidoDAO extends CrudRepository<Partido, Integer>{
	@Query ("SELECT p FROM Partido p WHERE p.torneo = :torneo AND NOT (p.status = 'CANC' OR p.status = 'PST') ORDER BY p.inicio ASC")
	List<Partido> findByTorneoNotCancelledOrderByInicio(Torneo torneo);
	
	@Query ("SELECT p FROM Partido p WHERE p.torneo = :torneo AND p.resultadoFinalEquipo1 is not null AND resultadoFinalEquipo2 is not null AND resultadoFinalEquipo1 >=0 AND resultadoFinalEquipo2 >=0 ORDER BY p.inicio DESC")
	List<Partido> findByTorneoWithResultOrderByInicioDesc(@Param("torneo")Torneo torneo);
	
	List<Partido> findByTorneoOrderByInicio(Torneo torneo);
	
	@Query ("SELECT DISTINCT p FROM Partido p WHERE p.torneo IN :torneos AND p.inicio BETWEEN :start AND :end AND NOT (p.status = 'CANC' OR p.status = 'PST')")
	List<Partido> findDistinctByTorneoInAndInicioBetweenNotCancelled(List<Torneo> torneos, Date start, Date end);
	
	List<Partido> findDistinctByTorneoInAndInicioBetween(List<Torneo> torneos, Date start, Date end);

	@Modifying(clearAutomatically = true, flushAutomatically = true)
	@Query("UPDATE Partido SET resultadoFinalEquipo1=:resultado1, resultadoFinalEquipo2=:resultado2 WHERE idPartido=:id")
	void updateResultados(@Param("id") Integer idPartido, @Param("resultado1") Integer resultadoFinalEquipo1, @Param("resultado2") Integer resultadoFinalEquipo2);
	
	Optional<Partido> findByExternalId(Integer externalId);

	@Query("SELECT p FROM Partido p WHERE p.esFinal = false AND p.inicio BETWEEN :betweenStart AND :betweenEnd AND NOT (p.status = 'CANC' OR p.status = 'PST') ")
	List<Partido> findPartidosLive(@Param("betweenStart")Date betweenStart, @Param("betweenEnd")Date betweenEnd);
}
