package com.pollapp.dao;


import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pollapp.model.Pais;

public interface PaisDAO extends CrudRepository<Pais, Integer>{
	public Optional<Pais> findByExternalId(String externalId);
	public Iterable<Pais> findAllByOrderByName();
}
