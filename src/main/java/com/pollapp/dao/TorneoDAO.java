package com.pollapp.dao;


import org.springframework.data.repository.CrudRepository;

import com.pollapp.model.Torneo;

public interface TorneoDAO extends CrudRepository<Torneo, Integer>{
	public Iterable<Torneo> findByIsActive(boolean active);
}
