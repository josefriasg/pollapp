package com.pollapp.batch.rapidapi;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.pollapp.batch.rapidapi.model.ApiFixture;
import com.pollapp.batch.rapidapi.model.ApiTeam;
import com.pollapp.batch.rapidapi.model.pojo.Fixture;
import com.pollapp.batch.rapidapi.model.pojo.Team;
import com.pollapp.config.AppConfiguration;
import com.pollapp.dao.EquipoDAO;
import com.pollapp.dao.PaisDAO;
import com.pollapp.dao.PartidoDAO;
import com.pollapp.dao.TorneoDAO;
import com.pollapp.model.Equipo;
import com.pollapp.model.Pais;
import com.pollapp.model.Partido;
import com.pollapp.model.Torneo;
import com.pollapp.service.impl.UsuarioServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Service
public class DataUpdaterRapidApi {
	
	
	private static String MATCH_FINISHED="FT";
	private static String MATCH_CANCELLED="CANC";
	
	private static String TIMEZONE="Europe/London";

	@Autowired
	private PartidoDAO partidoDAO;
	
	@Autowired
	private TorneoDAO torneoDAO;

	@Autowired
	private EquipoDAO equipoDAO;
	
	@Autowired
	private PaisDAO paisDAO;
	
	@Autowired
	private AppConfiguration config;
	
	//private static Client client = ClientBuilder.newClient();;
	
	private static String rapidapiHost="api-football-v1.p.rapidapi.com";
	
	@Scheduled(cron = "${app.equipoUpdaterScheduledCron}")
	@Transactional
	public void updateEquipos() {
		/*
		 * 1. Los torneos que quiero ponerles un campo con el codigo de RAPIDAPI
		 * 2. Que una vez a la semana/mes? revise los torneos y actualice los equipos (cambia cada temporada). con nuevo campo para id team ede RapidAPI
		 * 
		 * OkHttpClient client = new OkHttpClient();

			Request request = new Request.Builder()
				.url("https://api-football-v1.p.rapidapi.com/v2/teams/league/2")
				.get()
				.addHeader("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
				.addHeader("x-rapidapi-key", "a77ecda5a7msh4a3974e0825e226p1bdefcjsn0df97a03020e")
				.build();
			
			Response response = client.newCall(request).execute();
		 * */
		
			
		
		log.info("Actualizando equipos");
		HttpHeaders headers = new HttpHeaders();
	    headers.add("x-rapidapi-host", DataUpdaterRapidApi.rapidapiHost);
		headers.add("x-rapidapi-key", config.getApplicationKey());
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
	    
		Iterable<Torneo> torneos = torneoDAO.findAll();
		for (Torneo torneo : torneos) {
			try {
				if (torneo.getExternalId()!=null && torneo.getExternalId()>0 && torneo.isActive()) {
					Integer torneoId = torneo.getExternalId();
					RestTemplate restTemplate = new RestTemplate();
					String url = "https://"+DataUpdaterRapidApi.rapidapiHost+"/v2/teams/league/";
					ResponseEntity<ApiTeam> response = restTemplate.exchange(url + torneoId, HttpMethod.GET, entity, ApiTeam.class);
					log.info("*************");
					log.info("*************");
					log.info("LLAMADOS FALTANTES A API"+response.getHeaders().get("X-RateLimit-requests-Remaining"));
					this.createMissingTeams(response.getBody());
				}	
			
			}catch (Throwable t) {
				log.error("Error actualizando equipos", t);
			}
		}
		
	}
	
	private void createMissingTeams(ApiTeam body) {
		List<Team> apiTeam= body.getApi().getTeams();
		for (Team team:apiTeam) {
			Optional<Equipo> equipos = this.equipoDAO.findByExternalId(team.getTeamId());
			if (!equipos.isPresent()) {
				Equipo equipo=new Equipo();
				equipo.setExternalId(team.getTeamId());
				equipo.setName(team.getName());
				equipo.setUrlEscudo(team.getLogo());
				Optional<Pais> pais = this.paisDAO.findByExternalId(team.getCountry());
				equipo.setPais(pais.orElse(null));
				log.info("Guardando equipo"+team.getName());
				this.equipoDAO.save(equipo);
			}
			
		}
		
	}

	@Scheduled(cron = "${app.partidoUpdaterScheduledCron}")
	@Transactional
	public void updatePartidos() {
		/*
		 * 1. Los torneos que quiero ponerles un campo con el codigo de RAPIDAPI
		 * 2. Que una vez diaria actualice los partidos de los torneos y los actualice (next fixtures from torneos)
		 * 
		 * OkHttpClient client = new OkHttpClient();

			Request request = new Request.Builder()
				.url("https://api-football-v1.p.rapidapi.com/v2/fixtures/league/4?timezone=Europe%252FLondon")
				.get()
				.addHeader("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
				.addHeader("x-rapidapi-key", "a77ecda5a7msh4a3974e0825e226p1bdefcjsn0df97a03020e")
				.build();
			
			Response response = client.newCall(request).execute();
		 * */
		
			log.info("Actualizando partidos");
			HttpHeaders headers = new HttpHeaders();
		    //headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.add("x-rapidapi-host", DataUpdaterRapidApi.rapidapiHost);
			headers.add("x-rapidapi-key", config.getApplicationKey());
			
			HttpEntity<String> entity = new HttpEntity<String>(headers);
		    
			Iterable<Torneo> torneos = torneoDAO.findAll();
			for (Torneo torneo : torneos) {
				try {
					if (torneo.getExternalId()!=null && torneo.getExternalId()>0 && torneo.isActive()) {
						Integer torneoId = torneo.getExternalId();
						RestTemplate restTemplate = new RestTemplate();
						String url = "https://"+DataUpdaterRapidApi.rapidapiHost+"/v2/fixtures/league/"+torneoId;
						UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
						        .queryParam("timezone", DataUpdaterRapidApi.TIMEZONE);
						ResponseEntity<ApiFixture> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, ApiFixture.class);
						log.info("*************");
						log.info("*************");
						log.info("LLAMADOS FALTANTES A API"+response.getHeaders().get("X-RateLimit-requests-Remaining"));
						
							this.updateFixtures(response.getBody(), torneo);	
						
					}
				}catch (Throwable t) {
					log.error("Error actualizando torneo", t);
				}
				
			}
	}
	
	private void updateFixtures(ApiFixture body, Torneo torneo) {
		List<Fixture> apiFixture= body.getApi().getFixtures();
		if (apiFixture == null) {
			log.warn("Fixtures de ese torneo es null");
			return;
		}
		for (Fixture fixture:apiFixture) {
			try {
				log.info("Actualizando partido:"+fixture.getFixtureId()+"-"+fixture.getStatus());
				log.info("Equipos:"+fixture.getHomeTeam().getTeamName()+" vs "+fixture.getAwayTeam().getTeamName());
			}catch (Exception e) {
				log.warn("Error en dato de fixture", e);
			}
			
			if (!fixture.getStatusShort().equals(DataUpdaterRapidApi.MATCH_CANCELLED)) {
			
				Optional<Partido> partido = this.partidoDAO.findByExternalId(fixture.getFixtureId());
				if (partido.isPresent()) {
					log.info("Partido esta presente");
					Partido updPartido = partido.get();
					Date inicio = this.getDateFromEvent(fixture.getEventDate());
					updPartido.setInicio(inicio);
					if (fixture.getGoalsHomeTeam()!=null && fixture.getGoalsAwayTeam()!=null) {
						log.info("Actualizando resultado");
						updPartido.setResultadoFinalEquipo1(fixture.getGoalsHomeTeam());
						updPartido.setResultadoFinalEquipo2(fixture.getGoalsAwayTeam());
							
					}
					updPartido.setEsFinal(fixture.getStatusShort().equals(DataUpdaterRapidApi.MATCH_FINISHED));
					updPartido.setStatus(fixture.getStatusShort());
					log.info("Guardando partido");
					this.partidoDAO.save(updPartido);	
				
				}else {
					log.info("Partido no existe");
					Partido newPartido=new Partido();
					newPartido.setExternalId(fixture.getFixtureId());
					newPartido.setTorneo(torneo);
					Optional<Equipo> equipo1 = this.equipoDAO.findByExternalId(fixture.getHomeTeam().getTeamId());
					Optional<Equipo> equipo2 = this.equipoDAO.findByExternalId(fixture.getAwayTeam().getTeamId());
					
					if (equipo1.isPresent() && equipo2.isPresent()) {
						log.info("Los equipos existen");
						newPartido.setEquipo1(equipo1.get());
						newPartido.setEquipo2(equipo2.get());
						
						Date inicio = this.getDateFromEvent(fixture.getEventDate());
						newPartido.setInicio(inicio);
						newPartido.setResultadoFinalEquipo1(fixture.getGoalsHomeTeam()==null?-1:fixture.getGoalsHomeTeam());
						newPartido.setResultadoFinalEquipo2(fixture.getGoalsAwayTeam()==null?-1:fixture.getGoalsAwayTeam());
						newPartido.setEsFinal(fixture.getStatusShort().equals(DataUpdaterRapidApi.MATCH_FINISHED));
						newPartido.setStatus(fixture.getStatusShort());
						log.info("Guardando partido");
						this.partidoDAO.save(newPartido);	
					}
					
				}
			}
			
		}
	}

	private Date getDateFromEvent(String eventDate) {
		ZonedDateTime zonedDateTime = ZonedDateTime.parse(eventDate+"["+DataUpdaterRapidApi.TIMEZONE+"]");
		return Date.from(zonedDateTime.toInstant());
		
	}

	@Scheduled(fixedDelayString = "${app.resultadosUpdaterScheduledDelay}")
	@Transactional
	public void updateScoresAndPartido() {
		/*
		 * 1. busque si hay partidos en vivo en pollapp
		 * 2. si hay, llame al servicio fixtures/live
		 * 3. por cada partido en vivo, encuentre el fixture
		 * 4. actualice resultado y si ya se termino
		 * 
		 * OkHttpClient client = new OkHttpClient();

			Request request = new Request.Builder()
				.url("https://api-football-v1.p.rapidapi.com/v2/fixtures/live")
				.get()
				.addHeader("x-rapidapi-host", "api-football-v1.p.rapidapi.com")
				.addHeader("x-rapidapi-key", "a77ecda5a7msh4a3974e0825e226p1bdefcjsn0df97a03020e")
				.build();
			
			Response response = client.newCall(request).execute();
		 * */
		
		log.info("Actualizando resultados y partido");
		ZonedDateTime start = ZonedDateTime.now().minusHours(3);//tiempo despues de empezado en que se revisa el marcador
		ZonedDateTime end = ZonedDateTime.now().plusMinutes(10);//tiempo antes de empezar el partido en el que se inhabilita las predicciones
		
		List<Partido> liveGames = this.partidoDAO.findPartidosLive(Date.from(start.toInstant()), Date.from(end.toInstant()));
		if (!liveGames.isEmpty()) {
			log.info("***************");
			log.info("***************");
			log.info("ACTUALIZANDO LIVE SCORES:"+new Date());
			HttpHeaders headers = new HttpHeaders();
		    //headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.add("x-rapidapi-host", DataUpdaterRapidApi.rapidapiHost);
			headers.add("x-rapidapi-key", config.getApplicationKey());
			
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			RestTemplate restTemplate = new RestTemplate();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String today = format.format(new Date());
			String url = "https://"+DataUpdaterRapidApi.rapidapiHost+"/v2/fixtures/date/"+today;
			
			ResponseEntity<ApiFixture> response = restTemplate.exchange(url, HttpMethod.GET, entity, ApiFixture.class);
			log.info("*************");
			log.info("*************");
			log.info("Llamando:"+url);
			log.info("LLAMADOS FALTANTES A API"+response.getHeaders().get("X-RateLimit-requests-Remaining"));
			for (Partido partido:liveGames) {
				try {
					log.info("Partido:"+partido.getEquipo1().getName()+" vs "+partido.getEquipo2().getName()+" ExternalId:"+partido.getExternalId());
					if (partido.getExternalId()!=null && partido.getExternalId()>0) {
						log.info("Tiene external Id:"+partido.getExternalId());
						boolean found=false;
						List<Fixture> fixtures = response.getBody().getApi().getFixtures();
						for (Fixture fixture:fixtures) {
							if (partido.getExternalId().intValue()==fixture.getFixtureId().intValue()) {
								found=true;
								log.info("Fixture:"+fixture.getFixtureId() +" Status:"+fixture.getStatusShort() +" Resultado:"+fixture.getGoalsHomeTeam()+"-"+fixture.getGoalsAwayTeam());
								partido.setResultadoFinalEquipo1(fixture.getGoalsHomeTeam()==null?0:fixture.getGoalsHomeTeam());
								partido.setResultadoFinalEquipo2(fixture.getGoalsAwayTeam()==null?0:fixture.getGoalsAwayTeam());
								partido.setEsFinal(fixture.getStatusShort().equals(DataUpdaterRapidApi.MATCH_FINISHED));
								partido.setStatus(fixture.getStatusShort());
								this.partidoDAO.save(partido);
								log.info("Guardado");
								break;
							}
						}
						if (!found) {
							log.warn("El partido no se encontro en los live Games.");
						}
					}
				}catch (Throwable t) {
					log.error("Error actualizando partido en vivo",t);
				}
			}
		}
	}
}
