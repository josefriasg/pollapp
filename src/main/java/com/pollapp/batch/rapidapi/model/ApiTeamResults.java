package com.pollapp.batch.rapidapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.pollapp.batch.rapidapi.model.pojo.Team;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "results", "teams" })
public
class ApiTeamResults{
	@JsonProperty("results")
	private Integer results;
	@JsonProperty("teams")
	private List<Team> teams = null;

	@JsonProperty("results")
	public Integer getResults() {
		return results;
	}

	@JsonProperty("results")
	public void setResults(Integer results) {
		this.results = results;
	}

	@JsonProperty("teams")
	public List<Team> getTeams() {
		return teams;
	}

	@JsonProperty("teams")
	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}
}