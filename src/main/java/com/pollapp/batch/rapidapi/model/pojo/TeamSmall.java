package com.pollapp.batch.rapidapi.model.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"team_id",
"team_name",
"logo"
})
public class TeamSmall {
	@JsonProperty("team_id")
	private Integer teamId;
	@JsonProperty("team_name")
	private String teamName;
	@JsonProperty("logo")
	private String logo;

	@JsonProperty("team_id")
	public Integer getTeamId() {
	return teamId;
	}

	@JsonProperty("team_id")
	public void setTeamId(Integer teamId) {
	this.teamId = teamId;
	}

	@JsonProperty("team_name")
	public String getTeamName() {
	return teamName;
	}

	@JsonProperty("team_name")
	public void setTeamName(String teamName) {
	this.teamName = teamName;
	}

	@JsonProperty("logo")
	public String getLogo() {
	return logo;
	}

	@JsonProperty("logo")
	public void setLogo(String logo) {
	this.logo = logo;
	}


	
}
