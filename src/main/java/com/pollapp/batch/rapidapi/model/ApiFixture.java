package com.pollapp.batch.rapidapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"api"
})
public class ApiFixture {
	@JsonProperty("api")
	private ApiFixtureResults api;
	
	@JsonProperty("api")
	public ApiFixtureResults getApi() {
	return api;
	}

	@JsonProperty("api")
	public void setApi(ApiFixtureResults api) {
	this.api = api;
	}
}
