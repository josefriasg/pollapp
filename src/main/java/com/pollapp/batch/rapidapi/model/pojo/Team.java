package com.pollapp.batch.rapidapi.model.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "team_id", "name", "code", "logo", "country", "is_national", "founded", "venue_name",
		"venue_surface", "venue_address", "venue_city", "venue_capacity" })
public class Team {
	
	@JsonProperty("team_id")
	private Integer teamId;
	@JsonProperty("name")
	private String name;
	@JsonProperty("code")
	private Object code;
	@JsonProperty("logo")
	private String logo;
	@JsonProperty("country")
	private String country;
	@JsonProperty("is_national")
	private Boolean isNational;
	@JsonProperty("founded")
	private Integer founded;
	@JsonProperty("venue_name")
	private String venueName;
	@JsonProperty("venue_surface")
	private String venueSurface;
	@JsonProperty("venue_address")
	private String venueAddress;
	@JsonProperty("venue_city")
	private String venueCity;
	@JsonProperty("venue_capacity")
	private Integer venueCapacity;

	@JsonProperty("team_id")
	public Integer getTeamId() {
		return teamId;
	}

	@JsonProperty("team_id")
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("code")
	public Object getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(Object code) {
		this.code = code;
	}

	@JsonProperty("logo")
	public String getLogo() {
		return logo;
	}

	@JsonProperty("logo")
	public void setLogo(String logo) {
		this.logo = logo;
	}

	@JsonProperty("country")
	public String getCountry() {
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	@JsonProperty("is_national")
	public Boolean getIsNational() {
		return isNational;
	}

	@JsonProperty("is_national")
	public void setIsNational(Boolean isNational) {
		this.isNational = isNational;
	}

	@JsonProperty("founded")
	public Integer getFounded() {
		return founded;
	}

	@JsonProperty("founded")
	public void setFounded(Integer founded) {
		this.founded = founded;
	}

	@JsonProperty("venue_name")
	public String getVenueName() {
		return venueName;
	}

	@JsonProperty("venue_name")
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	@JsonProperty("venue_surface")
	public String getVenueSurface() {
		return venueSurface;
	}

	@JsonProperty("venue_surface")
	public void setVenueSurface(String venueSurface) {
		this.venueSurface = venueSurface;
	}

	@JsonProperty("venue_address")
	public String getVenueAddress() {
		return venueAddress;
	}

	@JsonProperty("venue_address")
	public void setVenueAddress(String venueAddress) {
		this.venueAddress = venueAddress;
	}

	@JsonProperty("venue_city")
	public String getVenueCity() {
		return venueCity;
	}

	@JsonProperty("venue_city")
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}

	@JsonProperty("venue_capacity")
	public Integer getVenueCapacity() {
		return venueCapacity;
	}

	@JsonProperty("venue_capacity")
	public void setVenueCapacity(Integer venueCapacity) {
		this.venueCapacity = venueCapacity;
	}

}
