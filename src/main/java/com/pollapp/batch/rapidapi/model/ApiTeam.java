package com.pollapp.batch.rapidapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"api"
})
public class ApiTeam {
	@JsonProperty("api")
	private ApiTeamResults api;
	
	@JsonProperty("api")
	public ApiTeamResults getApi() {
	return api;
	}

	@JsonProperty("api")
	public void setApi(ApiTeamResults api) {
	this.api = api;
	}
	
}




