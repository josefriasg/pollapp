package com.pollapp.batch.rapidapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.pollapp.batch.rapidapi.model.pojo.Fixture;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "results", "fixtures" })
public class ApiFixtureResults{
	@JsonProperty("results")
	private Integer results;
	@JsonProperty("fixtures")
	private List<Fixture> fixtures = null;

	@JsonProperty("results")
	public Integer getResults() {
		return results;
	}

	@JsonProperty("results")
	public void setResults(Integer results) {
		this.results = results;
	}

	@JsonProperty("fixtures")
	public List<Fixture> getFixtures() {
		return fixtures;
	}

	@JsonProperty("fixtures")
	public void setFixtures(List<Fixture> fixtures) {
		this.fixtures = fixtures;
	}
}
