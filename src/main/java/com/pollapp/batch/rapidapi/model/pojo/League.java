package com.pollapp.batch.rapidapi.model.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"name",
"country",
"logo",
"flag"
})
public class League {
	@JsonProperty("name")
	private String name;
	@JsonProperty("country")
	private String country;
	@JsonProperty("logo")
	private String logo;
	@JsonProperty("flag")
	private String flag;

	@JsonProperty("name")
	public String getName() {
	return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
	this.name = name;
	}

	@JsonProperty("country")
	public String getCountry() {
	return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
	this.country = country;
	}

	@JsonProperty("logo")
	public String getLogo() {
	return logo;
	}

	@JsonProperty("logo")
	public void setLogo(String logo) {
	this.logo = logo;
	}

	@JsonProperty("flag")
	public String getFlag() {
	return flag;
	}

	@JsonProperty("flag")
	public void setFlag(String flag) {
	this.flag = flag;
	}

}
