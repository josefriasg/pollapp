package com.pollapp.batch.reminder;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.dao.PartidoDAO;
import com.pollapp.dao.PollaDAO;
import com.pollapp.model.Partido;
import com.pollapp.model.Polla;
import com.pollapp.model.PrediccionPartido;
import com.pollapp.model.PrediccionesUsuario;
import com.pollapp.model.Torneo;
import com.pollapp.model.Usuario;
import com.pollapp.service.MailService;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Service
@Transactional
public class EmailReminder {
	
	@Autowired
	private PartidoDAO partidoDAO;
	
	@Autowired
	private PollaDAO pollaDAO;
	
	@Autowired
	private MailService mailService;
	
	@Scheduled(cron = "${app.emailReminderScheduledCron}")
	@Transactional
	public void sendReminders() {
		/*
		 * 1. Obtener los partidos que se juegan durante la semana
		 * 2. Obtener las pollas de los torneos a los que pertenecen los partidos
		 * 3. Obtener usuarios de esas pollas que no tienen prediccion en esos partidos
		 * 4. Consolidar por cada usuario todos los partidos que no tienen prediccion
		 * 5. Enviar email de recordatorio con listado de partidos y fecha/hora del partido
		 */
		
		log.info("Obteniendo partidos de la semana");
		ZonedDateTime start = ZonedDateTime.now();//tiempo despues de empezado en que se revisa el marcador
		ZonedDateTime end = ZonedDateTime.now().plusDays(7);//tiempo antes de empezar el partido en el que se inhabilita las predicciones
		
		List<Partido> weekGames = this.partidoDAO.findPartidosLive(Date.from(start.toInstant()), Date.from(end.toInstant()));
		
		List<Torneo> torneos = new ArrayList<Torneo>();
		weekGames.forEach(partido -> torneos.add(partido.getTorneo()));
		
		log.info("Obteniendo pollas con partidos en la semana");
		List<Polla> pollas = this.pollaDAO.findByTorneoIn(torneos);
		
		Map<Usuario, List<PartidosPolla>> users = new HashMap<>();
	
		for (Polla polla:pollas) {
			List<PrediccionesUsuario> predicUsuario = polla.getPrediccionesUsuario();
			for (PrediccionesUsuario predicUsu:predicUsuario) {
				if (predicUsu.getUsuario().getUserId()!=1 && predicUsu.getUsuario().isWantEmails()) {
					List<Partido> gamesNotPredicted = this.getGamesNotPredicted(predicUsu, weekGames);
					if (!gamesNotPredicted.isEmpty()) {
						log.info("El usuario tiene partidos sin prediccion:"+predicUsu.getUsuario().getEmail());
						PartidosPolla partidosPolla = new PartidosPolla(polla.getName(), gamesNotPredicted);
						if (users.containsKey(predicUsu.getUsuario())){
							List<PartidosPolla> partidosPollas =new ArrayList<PartidosPolla>();
							partidosPollas.add(partidosPolla);
							partidosPollas.addAll(users.get(predicUsu.getUsuario()));
							users.put(predicUsu.getUsuario(), partidosPollas);
						}else {
							users.put(predicUsu.getUsuario(), Arrays.asList(partidosPolla));
						}
					}
				}
			}
		}
		
		
		for (Usuario usuario:users.keySet()) {

			String body = this.buildBody(usuario,users.get(usuario));
			this.mailService.sendEmail(usuario.getEmail(), "Tienes partidos sin predicciones en PollApp!", body);
		}
		
	}

	private String buildBody(Usuario usuario, List<PartidosPolla> partidosPolla) {
		String body = "Hola "+usuario.getName()+"<br><br>"
				+"Vemos que no has ingresado tu predicción en PollApp para los siguientes partidos que se juegan esta semana:<br><br>";
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		
		for (PartidosPolla pollaFaltante:partidosPolla) {
			
			body +="<b>Polla:"+pollaFaltante.getPollaName()+"</b><br>";
			
			pollaFaltante.getPrediccionesFaltantes().sort((Partido x, Partido y) -> x.getInicio().compareTo(y.getInicio()));
			for (Partido faltante:pollaFaltante.getPrediccionesFaltantes()) {
				
				body += faltante.getEquipo1().getName() +" vs. "+faltante.getEquipo2().getName() +"   Fecha: "+sdf.format(faltante.getInicio())+"<br>";	
			}
			body +="<br>";
		}
		
		body +="<br>Te sugerimos ingresar a PollApp e ingresar tus predicciones. No des ventaja!<br><br>Atentamente,<br><br>Equipo de PollApp";
		return body;
	}

	private List<Partido> getGamesNotPredicted(PrediccionesUsuario prediccionUsu, List<Partido> weekGames) {
		List<Partido> notPredicted = new ArrayList<Partido>();
		for (Partido partido:weekGames) {
			if (partido.getTorneo().getIdTorneo()!=prediccionUsu.getPolla().getTorneo().getIdTorneo()) {
				continue;
			}
			boolean found = false;
			
			for (PrediccionPartido prediccion:prediccionUsu.getPrediccionPartido()) {
				if (prediccion.getPartido().getIdPartido() == partido.getIdPartido()) {
					found=true;
					break;
				}
			}
			if (!found) {
				notPredicted.add(partido);
			}
		}
		return notPredicted;
	}

	

}

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode
class PartidosPolla{
	private String pollaName;
	private List<Partido> prediccionesFaltantes;
	
}
