package com.pollapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class PrediccionPartido {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPrediccionPartido;
	
	@ManyToOne (fetch = FetchType.EAGER)
	private Partido partido;
	
	@Column
	private Integer resultadoEquipo1;
	
	@Column
	private Integer resultadoEquipo2;
}
