package com.pollapp.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class Torneo {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idTorneo;
	
	@Column
	private String name;
	
	@OneToMany(targetEntity=Partido.class, fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Set<Partido> partidos;
	
	@Column
	private Integer externalId;
	
	@Column(columnDefinition = "boolean default true")
	private boolean isActive;
	
}
