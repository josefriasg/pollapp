package com.pollapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode
@Entity
public class Partido {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPartido;
	
	@ManyToOne (fetch = FetchType.EAGER)
	private Equipo equipo1;
	
	@ManyToOne (fetch = FetchType.EAGER)
	private Equipo equipo2;

	@ManyToOne (fetch = FetchType.EAGER)
	private Torneo torneo;
	
	@Column
	private Integer resultadoFinalEquipo1;
	
	@Column
	private Integer resultadoFinalEquipo2;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date inicio; 
	
	@Column
	private Boolean esFinal;
	
	@Column
	private Integer externalId;
	
	@Column
	private String status;
}
