package com.pollapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class Pais {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPais;
	
	@Column
	private String name;
	
	@Column
	private String externalId;
	
}
