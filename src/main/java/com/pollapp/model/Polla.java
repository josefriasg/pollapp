package com.pollapp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class Polla {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPolla;
	
	@Column
	private String name;
	
	@ManyToOne (fetch = FetchType.LAZY)
	private Torneo torneo;
	
	
	@ManyToOne (fetch = FetchType.EAGER)
	private Usuario creador;
	
	@Column
	private String emailsInvitados;
	
	@Column
	private Integer puntosExactoEquipo;
	
	@Column
	private Integer puntosGanadorOEmpate;
	
	@Column
    private Integer puntosDiferencia;
	
	@Column
	private String uniqueCode;
	
	@OneToMany(targetEntity=PrediccionesUsuario.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<PrediccionesUsuario> prediccionesUsuario;
	
}
