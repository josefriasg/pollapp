package com.pollapp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class PrediccionesUsuario {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPrediccionesUsuario;
	
	@ManyToOne (fetch = FetchType.EAGER)
	private Usuario usuario;
	
	@ManyToOne (fetch = FetchType.EAGER)
	private Polla polla;
	
	@OneToMany(targetEntity=PrediccionPartido.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	List<PrediccionPartido> prediccionPartido;
}
