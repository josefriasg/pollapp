package com.pollapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.pollapp.model.enums.Rol;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class Usuario {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userId;
	
	@Column
	private String name;
	
	@Column(nullable = false, unique = true)
	private String email;
	
	@Column
	private String password;
	
	@Column
	private boolean isEnabled;
	
	@Column 
	private boolean isDropped;
	
	@Enumerated
	@Column
	private Rol role;
	
	@Column
	private byte[] picture;
	
	@Column(columnDefinition = "boolean default true")
	private boolean wantEmails;
	
	

	@Enumerated(EnumType.ORDINAL)
	public Rol getRole() {
		return role;
	}
}
