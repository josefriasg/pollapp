package com.pollapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class Comentario {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idComentario;
	
	@Column
	private String comentario;
	
	@ManyToOne (fetch = FetchType.EAGER)
	private Usuario usuario;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date curDate;
}
