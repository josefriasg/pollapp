package com.pollapp.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.pollapp.model.Usuario;

public interface UsuarioService {
	
		public List<Usuario> getAllUsuario();
		public Usuario saveUsuario(Usuario usuario);
		public void deleteUsuario(int usuarioId);
		public Usuario getUsuario(int usuarioId);
		public Usuario getUsuarioByEmail(String email);
		public void sendRegistrationEmail(Usuario user, String password);
		public boolean resetPassword(String email);
		public int uploadPicture(Usuario user, MultipartFile file)  throws IOException;
	
}
