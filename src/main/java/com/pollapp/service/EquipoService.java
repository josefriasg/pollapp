package com.pollapp.service;

import java.util.List;

import com.pollapp.model.Equipo;

public interface EquipoService {
	public Equipo saveEquipo (Equipo equipo);
	public List<Equipo> getAllEquipo ();
	public Equipo getEquipo(int idEquipo);
	public void deleteEquipo(int idEquipo);
	public List<Equipo> getAllEquipoForPais(Integer idPais);
}
