package com.pollapp.service;

import java.util.List;

import com.pollapp.dto.ResultadoOperacionDto;
import com.pollapp.model.Partido;

public interface PartidoService {
	public Partido savePartido (Partido partido);
	public List<Partido> getAllPartido ();
	public Partido getPartido(int idPartido);
	public void deletePartido(int idPartido);
	public List<Partido> getAllPartidoForTorneo(Integer idTorneo);
	public ResultadoOperacionDto saveMarcadores(Partido[] partidos);
}
