package com.pollapp.service;

import java.util.Date;
import java.util.List;

import com.pollapp.dto.EventoCalendario;
import com.pollapp.model.Partido;
import com.pollapp.model.Polla;
import com.pollapp.model.PrediccionPartido;
import com.pollapp.model.PrediccionesUsuario;
import com.pollapp.model.Usuario;

public interface PollaService {
	public Polla savePolla (Polla polla);
	public List<Polla> getAllPolla (Usuario usuario);
	public Polla getPolla(int idPolla);
	public void deletePolla(int idPolla);
	public int addUserToPolla(Usuario user, String codigoPolla);
	public int savePredicciones(Integer idPolla, List<PrediccionPartido> predicciones, Usuario user);
	public List<PrediccionPartido> getPrediccionesPolla(Integer idPolla, Usuario user);
	public List<PrediccionPartido> getPrediccionesPolla(Integer idPolla, String action, Usuario user);
	public List<EventoCalendario> getAllPrediccionesMonth(Usuario user, Date viewDate);
	public List<Partido> getPartidosEmpezadosFromPolla(Integer idPolla);
	public List<PrediccionesUsuario> getPrediccionesPartidoPolla(Integer idPolla, Integer idPartido);
}
