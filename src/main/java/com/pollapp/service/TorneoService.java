package com.pollapp.service;

import java.util.List;

import com.pollapp.model.Torneo;

public interface TorneoService {
	public Torneo saveTorneo (Torneo torneo);
	public List<Torneo> getAllTorneo ();
	public List<Torneo> getAllTorneoActive ();
	public Torneo getTorneo(int idTorneo);
	public void deleteTorneo(int idTorneo);
}
