package com.pollapp.service;

import java.util.List;

import com.pollapp.model.Pais;

public interface PaisService {
	public Pais savePais (Pais pais);
	public List<Pais> getAllPais ();
	public Pais getPais(int idPais);
	public void deletePais(int idPais);
}
