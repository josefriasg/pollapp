package com.pollapp.service;

import java.util.List;

import com.pollapp.model.Deporte;

public interface DeporteService {
	public Deporte saveDeporte (Deporte deporte);
	public List<Deporte> getAllDeporte ();
	public Deporte getDeporte(int idDeporte);
	public void deleteDeporte(int idDeporte);
}
