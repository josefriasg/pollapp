package com.pollapp.service;

import java.util.List;

import com.pollapp.model.Comentario;
import com.pollapp.model.Usuario;


public interface ComentarioService {
	public Comentario saveComentario (String comentario, Usuario usuario);
	public List<Comentario> getAllComentario();

}
