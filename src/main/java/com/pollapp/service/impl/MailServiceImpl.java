package com.pollapp.service.impl;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.service.MailService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class MailServiceImpl implements MailService {
	
	private static final String EMAIL_FROM="support@poll-app.net";
	private static final String EMAIL_NAME_FROM="Soporte PollApp";
	
	@Autowired
	private JavaMailSender emailSender;

	@Async
	public void sendEmail(String recipient, String subject, String body) {
		
		log.info("Sending email to:"+recipient);
		
		try {
			MimeMessage message = emailSender.createMimeMessage();
			  
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			 
			helper.setTo(recipient);
			helper.setFrom(MailServiceImpl.EMAIL_FROM, MailServiceImpl.EMAIL_NAME_FROM);
			helper.setReplyTo(MailServiceImpl.EMAIL_FROM, MailServiceImpl.EMAIL_NAME_FROM);
			helper.setSubject(subject);
			helper.setText(body, true);

			emailSender.send(message);
			log.info("Email sent");
		} catch (MailException e) {
			log.error("Error sending email", e);
		} catch (MessagingException e) {
			log.error("Error sending email", e);
		} catch (UnsupportedEncodingException e) {
			log.error("Error sending email", e);
		}
	}
	
	@Async
	public void sendEmail(String[] recipients, String subject, String body) {
		
		log.info("Sending email to many:"+recipients);
		
		try {
			MimeMessage message = emailSender.createMimeMessage();
			  
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			 
			helper.setTo(recipients);
			helper.setFrom(MailServiceImpl.EMAIL_FROM);
			helper.setSubject(subject);
			helper.setText(body, true);

			emailSender.send(message);
			log.info("Email sent");
		} catch (MailException e) {
			log.error("Error sending email", e);
		} catch (MessagingException e) {
			log.error("Error sending email", e);
		}
	}

}
