package com.pollapp.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.dao.PartidoDAO;
import com.pollapp.dao.TorneoDAO;
import com.pollapp.dto.ResultadoOperacionDto;
import com.pollapp.model.Partido;
import com.pollapp.model.Torneo;
import com.pollapp.service.PartidoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class PartidoServiceImpl implements PartidoService {

	@Autowired
	private PartidoDAO partidoDAO;
	
	@Autowired
	private TorneoDAO torneoDAO;
	
	@Override
	public Partido savePartido(Partido partido) {
		log.info("Guardar partido");
		return partidoDAO.save(partido);
	}

	@Override
	public List<Partido> getAllPartido() {
		log.info("Obtener todos los partidos");
		Iterable<Partido> iterable = partidoDAO.findAll();
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}

	@Override
	public Partido getPartido(int idPartido) {
		log.info("Obtener partido");
		return partidoDAO.findById(idPartido).orElse(null);
	}

	@Override
	public void deletePartido(int idPartido) {
		log.info("Borrar partido");
		partidoDAO.deleteById(idPartido);
		
	}

	@Override
	public List<Partido> getAllPartidoForTorneo(Integer idTorneo) {
		log.info("Obtener partidos de torneo");
		Torneo torneo = torneoDAO.findById(idTorneo).orElse(null);
		return torneo!=null?partidoDAO.findByTorneoNotCancelledOrderByInicio(torneo):null;	
	}

	@Override
	public ResultadoOperacionDto saveMarcadores(Partido[] partidos) {
		log.info("Guardar marcadores");
		for (Partido partido : partidos) {
			if (partido.getResultadoFinalEquipo1()!=null && partido.getResultadoFinalEquipo1()>=0 && partido.getResultadoFinalEquipo2()!=null && partido.getResultadoFinalEquipo2()>=0) {
				partidoDAO.updateResultados(partido.getIdPartido(), partido.getResultadoFinalEquipo1(), partido.getResultadoFinalEquipo2());
			}
		}
		return new ResultadoOperacionDto(0,"Marcadores actualizados");
	}

}
