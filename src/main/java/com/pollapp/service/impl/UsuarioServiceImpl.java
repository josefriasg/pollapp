package com.pollapp.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputField;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Rotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.pollapp.controller.UsuarioController;
import com.pollapp.dao.UsuarioDAO;
import com.pollapp.model.Usuario;
import com.pollapp.model.enums.Rol;
import com.pollapp.service.MailService;
import com.pollapp.service.UsuarioService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {
	
	
	@Autowired
	private UsuarioDAO usuarioDao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private MailService mailService;
	
	Random random = new SecureRandom();
	
	private static int IMAGE_WIDTH=100;
	private static final int ORIENTATION = 0x0112;

//	public void setUsuarioDao(UsuarioDAO usuarioDao) {
//		this.usuarioDao = usuarioDao;
//	}

	@Transactional
	public List<Usuario> getAllUsuario() {
		log.info("Obtener todos los usuarios");
		return (List<Usuario>)usuarioDao.findAll();
	}

	@Transactional
	public Usuario saveUsuario(Usuario usuario) {
		log.info("Guardar usuario");
		boolean isNew=false;
		if (usuario.getUserId()==null || (usuario.getUserId()!=null && usuario.getUserId()<=0)) {
			log.info("Usuario es nuevo");
			isNew=true;
			usuario.setUserId(null);
		}
		
		if (isNew) {
			String password = generateRandomPassword();
			//TODO quitar esto
			//password = "josedu82";
			usuario.setPassword(passwordEncoder.encode(password));
			usuario.setDropped(false);
			usuario.setEnabled(true);
			log.info("Enviando email");
			this.sendRegistrationEmail(usuario, password);
		}
		
		usuario.setEmail(usuario.getEmail().toLowerCase());
		log.info("Guardando usuario");
		Usuario saved =usuarioDao.save(usuario); 
		if (saved.getUserId()==1) {
			log.info("Usuario es administrador");
			saved.setRole(Rol.ADMIN);
			saved = usuarioDao.save(saved); 
		}
		return saved;
	}

	private String generateRandomPassword() {
		log.info("Generando password");
        Stream<Character> pwdStream = Stream.concat(getRandomNumbers(2), Stream.concat(getRandomAlphabets(2, true), getRandomAlphabets(4, false)));
        List<Character> charList = pwdStream.collect(Collectors.toList());
        Collections.shuffle(charList);
        String password = charList.stream()
            .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
            .toString();
        return password;
    }

    

    private Stream<Character> getRandomAlphabets(int count, boolean upperCase) {
        IntStream characters = null;
        if (upperCase) {
            characters = random.ints(count, 65, 90);
        } else {
            characters = random.ints(count, 97, 122);
        }
        return characters.mapToObj(data -> (char) data);
    }

    private Stream<Character> getRandomNumbers(int count) {
        IntStream numbers = random.ints(count, 48, 57);
        return numbers.mapToObj(data -> (char) data);
    }

//    private Stream<Character> getRandomSpecialChars(int count) {
//        IntStream specialChars = random.ints(count, 33, 45);
//        return specialChars.mapToObj(data -> (char) data);
//    }

	@Transactional
	public void deleteUsuario(int usuarioId) {
		log.info("Borrar usuario");
		usuarioDao.deleteById(usuarioId);
	}

	@Transactional
	public Usuario getUsuario(int usuarioId) {
		log.info("Obtener usuario");
		Optional<Usuario> retValue = usuarioDao.findById(usuarioId);
		if (retValue.isPresent()) {
			log.info("Usuario existe");
			return retValue.get();
		}
		log.warn("Usuario no existe");
		return null;
	}

	@Override
	public Usuario getUsuarioByEmail(String email) {
		log.info("Obtener usuario por email:"+email);
		return usuarioDao.findByEmail(email).orElse(null);
		
	}

	@Override
	public void sendRegistrationEmail(Usuario user, String password) {
		log.info("Enviar email de registro");
		String emailBody = this.buildBody(user, password);
		//TODO CAMBIAR POR UNA CUENTA DE TOA
		this.mailService.sendEmail(user.getEmail(), "Bienvenido a PollApp!", emailBody);
		
	}
	
	
	
	private String buildBody(Usuario user, String password) {
		String body = "Hola "+user.getName()+
				"<br/><br/>Tu cuenta en PollApp ha sido activada. Tu contraseña es:<br/>"
				+ "<h2>"+password+"</h2>"
				+ "<br/><br/>Con ella y tu email, puedes ingresar al siguiente link y empezar a jugar<br/><br/"
				+"<a>http://pollapp.us-east-2.elasticbeanstalk.com/</a>"
				+"<br/><br/>Una vez hayas ingresado, puedes cambiar la contraseña por una de tu agrado"
				+"<br/><br/>Gracias";
		
		return body;
	}
	
	private void sendResetEmail(Usuario user, String password) {
		log.info("Enviar email de reset:"+user.getEmail());
		String emailBody = this.buildResetBody(user, password);
		this.mailService.sendEmail(user.getEmail(), "Nueva contraseña en PollApp!", emailBody);
		
	}
	
	private String buildResetBody(Usuario user, String password) {
		String body = "Hola "+user.getName()+
				"<br/><br/>Hemos recibido una solicitud para generar una nueva contraseña en PollApp. Tu nueva contraseña es:<br/>"
				+ "<h2>"+password+"</h2>"
				+ "<br/><br/>Con ella y tu email, puedes ingresar al siguiente link y empezar a jugar<br/><br/"
				+"<a>http://www.poll-app.net</a>"
				+"<br/><br/>Una vez hayas ingresado, puedes cambiar la contraseña por una de tu agrado"
				+"<br/><br/>Gracias";
		
		return body;
	}

//	private void sendEmail(String recipient, String from, String subject, String body) {
//		log.debug("Sending registration email to:"+recipient);
//		
//		try {
//			MimeMessage message = emailSender.createMimeMessage();
//			  
//			MimeMessageHelper helper = new MimeMessageHelper(message, true);
//			 
//			helper.setTo(recipient);
//			helper.setFrom(from);
//			helper.setSubject(subject);
//			helper.setText(body, true);
//
//			emailSender.send(message);
//		} catch (MailException e) {
//			log.error("Error sending email", e);
//		} catch (MessagingException e) {
//			log.error("Error sending email", e);
//		}
//	}

	@Override
	public boolean resetPassword(String email) {
		log.info("Reseteando password:"+email);
		Usuario user = usuarioDao.findByEmail(email).orElse(null);
		if (user!=null) {
			log.info("Usuario encontrado");
			String password = generateRandomPassword();
			user.setPassword(passwordEncoder.encode(password));
			this.sendResetEmail(user, password);
			log.info("Guardando usuario");
			usuarioDao.save(user);
			return true;
		}
		log.warn("User not found");
		return false;
	}

	@Override
	public int uploadPicture(Usuario user, MultipartFile file) throws IOException {
//		File input = new File(file.getOriginalFilename());
//		file.transferTo(input);
//		File output = new File("Rotated"+file.getOriginalFilename());
//		try {
//			output = ExifImageUtils.rotateFromOrientationData(input, output);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		int orientation=-1;
		try {
			orientation = this.rotateFromOrientationData(file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] thumbnail = this.createThumbnail(file, orientation).toByteArray();
        user.setPicture(thumbnail);
        this.saveUsuario(user);
		return 0;
	}
	
	private int rotateFromOrientationData(MultipartFile inputFile) throws Exception{
		ImageMetadata metadata = Imaging.getMetadata(inputFile.getBytes());

        if (!(metadata instanceof JpegImageMetadata)) {
            return -1;
        }

        JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

        final TiffImageMetadata exif = jpegMetadata.getExif();

        if (exif == null) {
            return -1;
        }

        TiffOutputSet outputSet = exif.getOutputSet();
        
        TiffOutputDirectory exifDirectory = outputSet.getOrCreateRootDirectory();

        TiffOutputField orientationField = exifDirectory.findField(ORIENTATION);

        if (orientationField == null) {
            return -1;
        }
        
        Iterator<TiffField> itField = exif.getAllFields().iterator();
        
        while (itField.hasNext()) {
        	TiffField field = itField.next();
        	String info = field.getDescriptionWithoutValue();
        	if (info.startsWith("274")) {//274 (0x112: Orientation): 6 (1 Short)
        		return field.getIntValue();
        	}
        }

        return -1;
	}

	private ByteArrayOutputStream createThumbnail(MultipartFile file, int orientation) throws IOException{  
	    ByteArrayOutputStream thumbOutput = new ByteArrayOutputStream();  
	     
	    BufferedImage img = ImageIO.read(file.getInputStream()); 
	    
	    //rotar
	    switch (orientation) {
        case 3://rotar 180
        	img = Scalr.rotate(img, Rotation.CW_180);
        	break;
        case 6://rotar 90 clock
        	img = Scalr.rotate(img, Rotation.CW_90);
        	break;
        case 8://rotar 90 counterclock
        	img = Scalr.rotate(img, Rotation.CW_270);
        	break;
        default:
        }
        
	    //corta la imagen para cuadrado mas grande posible del centro
	    int pixels =Math.min(img.getHeight(), img.getWidth());
	    log.debug("Cropping image to largest square center crop : {} pixels", pixels);
	    img = Scalr.crop(img, (img.getWidth() - pixels) / 2, (img.getHeight() - pixels) / 2, pixels, pixels);
	    
	    //reajusta a 100x100
	    BufferedImage thumbImg = Scalr.resize(img, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.AUTOMATIC, UsuarioServiceImpl.IMAGE_WIDTH, Scalr.OP_ANTIALIAS);
	 
	    ImageIO.write(thumbImg, file.getContentType().split("/")[1] , thumbOutput);
	    log.debug("Thumbnail Byte Size - " + thumbOutput.toByteArray().length);
		
	    return thumbOutput;  
	}

}
