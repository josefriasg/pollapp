package com.pollapp.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.dao.EquipoDAO;
import com.pollapp.dao.PaisDAO;
import com.pollapp.model.Equipo;
import com.pollapp.model.Pais;
import com.pollapp.service.EquipoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class EquipoServiceImpl implements EquipoService {

	@Autowired
	private EquipoDAO equipoDAO;
	
	@Autowired
	private PaisDAO paisDAO;
	
	@Override
	public Equipo saveEquipo(Equipo equipo) {
		log.info("Guardar equipo");
		return equipoDAO.save(equipo);
	}

	@Override
	public List<Equipo> getAllEquipo() {
		log.info("Obtener todos los equipos");
		Iterable<Equipo> iterable = equipoDAO.findAll();
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}

	@Override
	public Equipo getEquipo(int idEquipo) {
		log.info("Obtener equipo");
		return equipoDAO.findById(idEquipo).orElse(null);
	}

	@Override
	public void deleteEquipo(int idEquipo) {
		log.info("Borrar equipo");
		equipoDAO.deleteById(idEquipo);
		
	}

	@Override
	public List<Equipo> getAllEquipoForPais(Integer idPais) {
		log.info("Obtener todos lequipo");
		Pais pais = paisDAO.findById(idPais).orElse(null);
		return pais!=null?equipoDAO.findByPaisOrderByName(pais):null;	
	}

}
