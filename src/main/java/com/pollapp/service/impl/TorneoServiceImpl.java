package com.pollapp.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.dao.TorneoDAO;
import com.pollapp.model.Torneo;
import com.pollapp.service.TorneoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class TorneoServiceImpl implements TorneoService {
	
	
	@Autowired
	private TorneoDAO torneoDAO;
	
	@Override
	public Torneo saveTorneo(Torneo torneo) {
		log.info("Guardar torneo");
		return torneoDAO.save(torneo);
	}

	@Override
	public List<Torneo> getAllTorneo() {
		log.info("Obtener todos los torneos");
		Iterable<Torneo> iterable = torneoDAO.findAll();
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}
	
	@Override
	public List<Torneo> getAllTorneoActive() {
		log.info("Obtener todos los torneos activos");
		Iterable<Torneo> iterable = torneoDAO.findByIsActive(true);
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}

	@Override
	public Torneo getTorneo(int idTorneo) {
		log.info("Obtener un torneo");
		return torneoDAO.findById(idTorneo).orElse(null);
	}

	@Override
	public void deleteTorneo(int idTorneo) {
		log.info("Borrar torneo");
		torneoDAO.deleteById(idTorneo);
		
	}

}
