package com.pollapp.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.pollapp.dao.UsuarioDAO;
import com.pollapp.model.Usuario;
import com.pollapp.service.impl.util.MyUserPrincipal;

public class MyUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UsuarioDAO usuarioDao;
	//private UsuarioCustomDAO usuarioCustomDao;
	
	public void setUsuarioDao(UsuarioDAO usuarioDao) {
		this.usuarioDao = usuarioDao;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioDao.findByEmail(username.toLowerCase()).orElse(null);
		if (usuario == null) {
			return null;
		}
		return new MyUserPrincipal(usuario);
		
	}

}
