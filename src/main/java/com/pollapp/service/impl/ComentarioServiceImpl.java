package com.pollapp.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.dao.ComentarioDAO;
import com.pollapp.dao.DeporteDAO;
import com.pollapp.model.Comentario;
import com.pollapp.model.Deporte;
import com.pollapp.model.Usuario;
import com.pollapp.service.ComentarioService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class ComentarioServiceImpl implements ComentarioService{
	
	@Autowired
	private ComentarioDAO comentarioDAO;

	public Comentario saveComentario(String comentarioTxt, Usuario usuario) {
		log.info("Guardar comentario");
		Comentario comentario = new Comentario(null, comentarioTxt, usuario, new Date());
		return comentarioDAO.save(comentario);
	}

	@Override
	public List<Comentario> getAllComentario() {
		log.info("Obtener todos los comentarios");
		Iterable<Comentario> iterable = comentarioDAO.findAllByOrderByCurDateDesc();
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}

}
