package com.pollapp.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.dao.PaisDAO;
import com.pollapp.model.Pais;
import com.pollapp.service.PaisService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class PaisServiceImpl implements PaisService {

	@Autowired
	private PaisDAO paisDAO;
	
	@Override
	public Pais savePais(Pais pais) {
		log.info("Guardar pais");
		return paisDAO.save(pais);
	}

	@Override
	public List<Pais> getAllPais() {
		log.info("Obtener todos los paises");
		Iterable<Pais> iterable = paisDAO.findAllByOrderByName();
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}

	@Override
	public Pais getPais(int idPais) {
		log.info("Obtener pais");
		return paisDAO.findById(idPais).orElse(null);
	}

	@Override
	public void deletePais(int idPais) {
		log.info("Borrar pais");
		paisDAO.deleteById(idPais);
		
	}

}
