package com.pollapp.service.impl;

import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFutureCallbackRegistry;

import com.pollapp.dao.PartidoDAO;
import com.pollapp.dao.PollaDAO;
import com.pollapp.dao.PrediccionesUsuarioDAO;
import com.pollapp.dao.UsuarioDAO;
import com.pollapp.dto.EventoCalendario;
import com.pollapp.dto.PrediccionesUsuarioDto;
import com.pollapp.model.Partido;
import com.pollapp.model.Polla;
import com.pollapp.model.PrediccionPartido;
import com.pollapp.model.PrediccionesUsuario;
import com.pollapp.model.Torneo;
import com.pollapp.model.Usuario;
import com.pollapp.service.MailService;
import com.pollapp.service.PollaService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class PollaServiceImpl implements PollaService {

	
	@Autowired
	private PollaDAO pollaDAO;
	
	@Autowired
	private PrediccionesUsuarioDAO prediccionesUsuarioDAO;
	
	@Autowired
	private PartidoDAO partidoDAO;
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Autowired
	private MailService mailService;
	
	Random random = new SecureRandom();
	
	@Override
	public Polla savePolla(Polla polla) {
		log.info("Guardar polla");
		
		if (polla.getIdPolla()<=0) {//nueva
			log.info("Nueva polla");
			PrediccionesUsuario prediccion = new PrediccionesUsuario(null, polla.getCreador(), polla, null);
			
			List<PrediccionesUsuario> predicciones = new ArrayList<PrediccionesUsuario>();
			predicciones.add(prediccion);
			
			
			//agregar el usuario pollapp a todas
			Usuario pollapp = usuarioDAO.findById(new Integer(1)).orElseGet(null);
			
			if (pollapp != null) {
				log.info("Agregando usuario pollapp");
				PrediccionesUsuario prediccionPollapp = new PrediccionesUsuario(null, pollapp, polla, null);
				predicciones.add(prediccionPollapp);
			}
			
			polla.setPrediccionesUsuario(predicciones);
			
			log.info("Guardando polla");
			Polla savedPolla = pollaDAO.save(polla);
			
			log.info("Generando codigo unico");
			String uniqueCode = generateUniqueCode(savedPolla);
			savedPolla.setUniqueCode(uniqueCode);
			
			log.info("Guardando polla otra vez");
			Polla updatedPolla= pollaDAO.save(savedPolla);
			
			log.info("Enviar correos");
			sendMails(updatedPolla);
			
			return updatedPolla;
				
		}else {//actualizar
			log.info("Guardar polla existente");
			return pollaDAO.save(polla);
		}
		
		
	}

	private void sendMails(Polla polla) {
		String recipient = polla.getEmailsInvitados();//checkEmails(polla.getEmailsInvitados());
		String subject = "Invitación a participar en poll-app.net";
		
		//TODO MEJORAR EL BODY
		String body = "Te han invitado a participar en una nueva polla. <br/><br/>"
				+ "Entra a "
				+ "<a>http://www.poll-app.net</a> <br/><br/> e ingresa el código "+polla.getUniqueCode()+" para empezar a jugar. <br/><br/>"
						+ "Atentamente, equipo de PollApp";
		if (recipient!=null && recipient.length()>0) {
			if (recipient.contains(",")) {
				String recipients[] = recipient.split(",");
				mailService.sendEmail(recipients, subject, body);
			}else {
				mailService.sendEmail(recipient, subject, body);	
			}
				
		}
		
	}

	private String checkEmails(String emailsInvitados) {
		// TODO Auto-generated method stub
		return null;
	}

	private String generateUniqueCode(Polla savedPolla) {
		//String encodedInfo = Base64.getUrlEncoder().encodeToString(savedPolla.getIdPolla().toString().getBytes());
		
//		MessageDigest md;
//		try {
//			md = MessageDigest.getInstance("MD5");
//			 md.update((savedPolla.getIdPolla()+savedPolla.getCreador().getEmail()).toString().getBytes());
//			    byte[] digest = md.digest();
//			    String myHash = DatatypeConverter
//			      .printHexBinary(digest).toUpperCase();
//			    return myHash;
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//			return null;
//		}
		
		Stream<Character> pwdStream = Stream.concat(getRandomNumbers(3), getRandomAlphabets(2, true));
        List<Character> charList = pwdStream.collect(Collectors.toList());
        Collections.shuffle(charList);
        String randomString = charList.stream()
            .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
            .toString();
        
        char[] idPollaArray = savedPolla.getIdPolla().toString().toCharArray();
        String idPollaReverse = "";
        for (char i:idPollaArray) {
        	idPollaReverse=i+idPollaReverse;
        }
        
        return randomString.substring(0, 3)+idPollaReverse+randomString.substring(3);
		
		
	}
	
	private Stream<Character> getRandomAlphabets(int count, boolean upperCase) {
        IntStream characters = null;
        if (upperCase) {
            characters = random.ints(count, 65, 90);
        } else {
            characters = random.ints(count, 97, 122);
        }
        return characters.mapToObj(data -> (char) data);
    }

    private Stream<Character> getRandomNumbers(int count) {
        IntStream numbers = random.ints(count, 48, 57);
        return numbers.mapToObj(data -> (char) data);
    }

	@Override
	public List<Polla> getAllPolla(Usuario usuario) {
		log.info("Obtener todas las pollas del usuario "+usuario.getEmail());
		Iterable<Polla> iterable = pollaDAO.getPollasByParticipante(usuario);
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}

	@Override
	public Polla getPolla(int idPolla) {
		log.info("Obtener  polla");
		return pollaDAO.findById(idPolla).orElse(null);
	}

	@Override
	public void deletePolla(int idPolla) {
		log.info("Borrar polla");
		pollaDAO.deleteById(idPolla);
		
	}

	@Override
	public int addUserToPolla(Usuario user, String codigoPolla) {
		log.info("Agregar usuario a polla");
		
		List<Polla> pollas = pollaDAO.findByUniqueCode(codigoPolla);
		
		if (pollas.size()>1) {
			log.warn("!!!!!!!!!MAS DE UNA POLLA CON EL CODIGO!!!!!!!!!!");
			return -2;
		}else if (pollas.isEmpty()) {
			log.info("No existe polla con ese codigo");
			return -1;
		}else {
		
			try {
				Polla polla = pollas.get(0);
				boolean alreadyInPolla = false;
				for (PrediccionesUsuario predicciones : polla.getPrediccionesUsuario()) {
					if (predicciones.getUsuario().getUserId() == user.getUserId()) {
						alreadyInPolla=true;
						break;
					}
				}
				if (!alreadyInPolla) {
					log.info("Agregando usuario");
					PrediccionesUsuario prediccion = new PrediccionesUsuario(null, user, polla, null);
					
					polla.getPrediccionesUsuario().add(prediccion);
					log.info("Guardando polla");
					pollaDAO.save(polla);
					
					return 0;	
				}else {
					log.info("Usuario ya esta en la polla");
					return -3;
				}
				
			} catch (Exception e) {
				log.error("Error agregando usuario a polla",e);
				return -2;
			}
		}
	}

	@Override
	public int savePredicciones(Integer idPolla, List<PrediccionPartido> predicciones, Usuario loggedUser) {
		log.info("Guardando predicciones");
		try {
			log.info("Buscando polla");
			Polla polla = this.pollaDAO.findById(idPolla).get();
			PrediccionesUsuario prediccionesUsuario = this.prediccionesUsuarioDAO.findByUsuarioAndPolla(loggedUser, polla).get(0);
		
			List<PrediccionPartido> newPrediccs = predicciones.stream().filter(prediccion -> isPrediccionValid(prediccion)).collect(Collectors.toList());
			prediccionesUsuario.getPrediccionPartido().size();
			List<PrediccionPartido> prevPrediccs = prediccionesUsuario.getPrediccionPartido();
			
			for (PrediccionPartido newPred:newPrediccs) {
				boolean foundPred=false;
				for (PrediccionPartido prevPred:prevPrediccs) {
					
					if (newPred.getPartido().getIdPartido().equals(prevPred.getPartido().getIdPartido())) {
						log.info("Encontro prediccion");
						foundPred=true;
						prevPred.setResultadoEquipo1(newPred.getResultadoEquipo1());
						prevPred.setResultadoEquipo2(newPred.getResultadoEquipo2());
						break;
					}
				}
				if (!foundPred) {
					log.info("No encontro prediccion, agregandola");
					prevPrediccs.add(newPred);
				}
			}
			
			log.info("Guardando prediccion");
			prediccionesUsuario.setPrediccionPartido(prevPrediccs);
			this.prediccionesUsuarioDAO.save(prediccionesUsuario);
			
		}catch (Exception e) {
			log.error("Error guardando prediccion", e);
			return -1;
		}
		
		log.info("Prediccion guardada");
		return 0;
	}
	
	private boolean isPrediccionValid(PrediccionPartido prediccion) {
		//TODO VALIDAR LA HORA Y NO GUARDAR SI YA EMPEZO EL PARTIDO
		return prediccion.getResultadoEquipo1() != null && prediccion.getResultadoEquipo1()>=0 && 
				prediccion.getResultadoEquipo2() != null && prediccion.getResultadoEquipo2()>=0;
	}

	@Override
	public List<PrediccionPartido> getPrediccionesPolla(Integer idPolla, Usuario user) {
		log.info("Obtener predicciones de polla y usuario "+user.getUserId());
		List<PrediccionPartido> retPrediccs = this.getPrediccionesPollaFromUser(idPolla, user);
		retPrediccs.sort((PrediccionPartido p1, PrediccionPartido p2) -> p2.getPartido().getInicio().compareTo(p1.getPartido().getInicio()));
		return retPrediccs;
	}
	
	@Override
	public List<EventoCalendario> getAllPrediccionesMonth(Usuario user, Date viewDate){
		/*1. obtener todas las predicciones del usuario en la que el partido esta en un mes especifico
		 * 2. obtener todos los partidos de todos los torneos de las polla en la que esta el usuario, en un mes especifico
		 * 2. hacer merge de ambas
		 * */
		
		log.info("Obtener predicciones para calendario para usuario y fecha "+user.getUserId() +"-"+viewDate);
		List<EventoCalendario> retEventos = new ArrayList<EventoCalendario>();
		
		Calendar calendar = GregorianCalendar.getInstance();
	    calendar.setTime(viewDate);
	    calendar.set(Calendar.DAY_OF_MONTH,
        calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
        Date start = calendar.getTime();
        
        calendar.set(Calendar.DAY_OF_MONTH,
        calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        Date end = calendar.getTime();
        
        log.info("Buscando predicciones del usuario entre "+start+" y "+end);
		List<PrediccionesUsuario> usuPrediccs = this.prediccionesUsuarioDAO.findDistinctByUsuarioAndPrediccionPartidoPartidoInicioBetween(user, start, end);
		
		
			for (PrediccionesUsuario usuPred:usuPrediccs) {
				List<PrediccionPartido> retPrediccs = usuPred.getPrediccionPartido();
				for (PrediccionPartido pred:retPrediccs) {
					log.info("Encontro predicciones");
					EventoCalendario evento= new EventoCalendario();
					evento.setStart(pred.getPartido().getInicio());
					Instant endTime = pred.getPartido().getInicio().toInstant().plus(2, ChronoUnit.HOURS);//TODO esto es solo futbol
					evento.setEnd(Date.from(endTime));
					String text = "Polla:"+usuPred.getPolla().getName()+"<br>"+pred.getPartido().getEquipo1().getName()+" vs "+pred.getPartido().getEquipo2().getName();
					if (pred.getResultadoEquipo1()!=null && pred.getResultadoEquipo2()!=null && 
							pred.getResultadoEquipo1()>=0 && pred.getResultadoEquipo2()>=0) {
						evento.setColor("blue");
						text+="<br>Predicción:"+pred.getResultadoEquipo1()+"-"+pred.getResultadoEquipo2();
						
					}else {
						evento.setColor("red");
					}
					if (pred.getPartido().getResultadoFinalEquipo1()!=null && pred.getPartido().getResultadoFinalEquipo2()!=null && 
							pred.getPartido().getResultadoFinalEquipo1()>=0 && pred.getPartido().getResultadoFinalEquipo2()>=0) {
						text+="<br>Resultado: "+pred.getPartido().getResultadoFinalEquipo1()+"-"+pred.getPartido().getResultadoFinalEquipo2();
					}
					log.info(text);
					evento.setText(text);
					retEventos.add(evento);
				}
			}
		
			
			List<Torneo> torneos = new ArrayList<Torneo>();
			log.info("Buscando todas las predicciones del usuario");
			List<PrediccionesUsuario> allUsuPrediccs = this.prediccionesUsuarioDAO.findByUsuario(user);
			
			for (PrediccionesUsuario usuPred:allUsuPrediccs) {
				torneos.add(usuPred.getPolla().getTorneo());
			}
			
			log.info("Buscando todos los partidos del torneo entre las fechas");
		List<Partido> partidos = partidoDAO.findDistinctByTorneoInAndInicioBetweenNotCancelled(torneos, start, end);
		for (Partido partido : partidos) {
			log.info("Hay partidos");
			for (PrediccionesUsuario usuPred:allUsuPrediccs) {
				boolean partidoAlreadyPredicted = false;
				Polla polla = this.pollaDAO.findById(usuPred.getPolla().getIdPolla()).get();
				if (!partido.getTorneo().getIdTorneo().equals(polla.getTorneo().getIdTorneo())) {
					continue;
				}
				log.info("El partido "+partido.getIdPartido()+" es de la polla "+polla.getIdPolla());
				List<PrediccionPartido> retPrediccs = usuPred.getPrediccionPartido();
				for (PrediccionPartido prediccion : retPrediccs) {
					if (prediccion.getPartido().getIdPartido() == partido.getIdPartido()) {
						log.info("El partido ya tiene prediccion");
						partidoAlreadyPredicted = true;
						break;
					}
				}
			
				if (!partidoAlreadyPredicted) {
					log.info("El partido no tiene prediccion");
					
						EventoCalendario evento= new EventoCalendario();
						evento.setColor("red");
						evento.setStart(partido.getInicio());
						Instant endTime = partido.getInicio().toInstant().plus(2, ChronoUnit.HOURS);//TODO esto es solo futbol
						evento.setEnd(Date.from(endTime));
						String text = "Polla:"+usuPred.getPolla().getName()+"<br>"+partido.getEquipo1().getName()+" vs "+partido.getEquipo2().getName();
						if (partido.getResultadoFinalEquipo1()!=null && partido.getResultadoFinalEquipo2()!=null && 
								partido.getResultadoFinalEquipo1()>=0 && partido.getResultadoFinalEquipo2()>=0) {
							text+="<br>Resultado: "+partido.getResultadoFinalEquipo1()+"-"+partido.getResultadoFinalEquipo2();
						}
						log.info(text);
						evento.setText(text);
						retEventos.add(evento);	
					
				}
			}
		}
		
		return retEventos;
	}
	
	

	@Override
	public List<PrediccionPartido> getPrediccionesPolla(Integer idPolla, String action, Usuario user) {
		log.info("Obtener predicciones de polla, accion y usuario "+idPolla+"-"+action+"-"+user.getUserId());
		List<PrediccionPartido> retPrediccs = this.getPrediccionesPollaFromUser(idPolla, user);
		
		if (action.equals("previous")) {
			log.info("Sacando los partidos que tengan resultado");
			//TODO filtar si ya tiene resultado o filtrar por fecha (antes de ahora, despues de ahora?)
			List<PrediccionPartido> previousPrediccs = retPrediccs.stream().filter(p -> p.getPartido().getResultadoFinalEquipo1()>=0 || p.getPartido().getResultadoFinalEquipo2()>=0).collect(Collectors.toList());
			previousPrediccs.sort((PrediccionPartido p1, PrediccionPartido p2) -> p2.getPartido().getInicio().compareTo(p1.getPartido().getInicio()));
			return previousPrediccs;
		}else if (action.equals("add")) {
			log.info("Sacando los partidos que no tengan resultado");
			List<PrediccionPartido> nextPrediccs = retPrediccs.stream().filter(p -> (p.getPartido().getResultadoFinalEquipo1() == null ||p.getPartido().getResultadoFinalEquipo1()<0) && (p.getPartido().getResultadoFinalEquipo2() == null || p.getPartido().getResultadoFinalEquipo2()<0)).collect(Collectors.toList());
			nextPrediccs.sort((PrediccionPartido p1, PrediccionPartido p2) -> p1.getPartido().getInicio().compareTo(p2.getPartido().getInicio()));
			return nextPrediccs;
		}else {
			log.warn("Accion no permitida");
			return new ArrayList<PrediccionPartido>();
		}
		
		
		
	}
	
	private List<PrediccionPartido> getPrediccionesPollaFromUser(Integer idPolla, Usuario user){
		List<PrediccionPartido> retPrediccs = new ArrayList<PrediccionPartido>();
		
		log.info("Obteniendo polla:"+idPolla);
		Polla polla = this.pollaDAO.findByIdAndFetchTorneoEagerly(idPolla).get(0);
		
		log.info("Obteniendo predicciones de la polla del usuario "+user.getUserId());
		List<PrediccionesUsuario> prediccionesUsuario = this.prediccionesUsuarioDAO.findByUsuarioAndPolla(user, polla);
		
		List<PrediccionPartido> prediccionesPartido =  prediccionesUsuario.get(0).getPrediccionPartido();
		
		log.info("Obtener partidos del torneo de la polla");
		List<Partido> partidos = partidoDAO.findByTorneoNotCancelledOrderByInicio(polla.getTorneo());
		
		retPrediccs.addAll(prediccionesPartido);
		
		for (Partido partido : partidos) {
			log.info("Hay partidos");
			boolean partidoAlreadyPredicted = false;
			for (PrediccionPartido prediccion : prediccionesPartido) {
				if (prediccion.getPartido().getIdPartido() == partido.getIdPartido()) {
					log.info("Partido ya tiene prediccion");
					partidoAlreadyPredicted = true;
					break;
				}
			}
			if (!partidoAlreadyPredicted) {
				log.info("Partido no tiene prediccion, agregando vacia");
				PrediccionPartido newPrediccion = new PrediccionPartido();
				newPrediccion.setPartido(partido);
				retPrediccs.add(newPrediccion);
			}
		}
		return retPrediccs;
	}

	@Override
	public List<Partido> getPartidosEmpezadosFromPolla(Integer idPolla) {
		log.info("Obteniendo polla:"+idPolla);
		
		
		
		Polla polla = this.pollaDAO.findById(idPolla).orElse(null);
		if (polla!=null) {
			List<Partido> partidos = partidoDAO.findByTorneoWithResultOrderByInicioDesc(polla.getTorneo());
			return partidos;
		}
		return null;
		
//		Polla polla = this.pollaDAO.findByIdAndFetchPartidosEagerly(idPolla).get(0);
//		polla.getTorneo().getPartidos().size();
//		Set<Partido> partidos = polla.getTorneo().getPartidos();
//		
//		partidos.removeIf(x -> x.getResultadoFinalEquipo1()==null || 
//				x.getResultadoFinalEquipo1()<0 || 
//				x.getResultadoFinalEquipo2()==null ||
//				x.getResultadoFinalEquipo2()<0);
//		
//		//Comparator<Partido> comparator = (p1, p2) -> p2.getInicio().compareTo(p1.getInicio());
//		List<Partido> retPartidos = partidos.stream().sorted(Comparator.comparing(Partido::getInicio, Comparator.reverseOrder())).collect(Collectors.toList());
//		return retPartidos;
	}

	@Override
	public List<PrediccionesUsuario> getPrediccionesPartidoPolla(Integer idPolla, Integer idPartido) {
		log.info("Obteniendo predicciones de polla:"+idPolla+" partido:"+idPartido);
		Optional<Polla> polla = pollaDAO.findById(idPolla);
		Optional<Partido> partido = partidoDAO.findById(idPartido);
		
		if (polla.isPresent() && partido.isPresent()) {// vacio si partido no ha empezado
			if (partido.get().getResultadoFinalEquipo1()==null || 
					partido.get().getResultadoFinalEquipo1()<0 || 
					partido.get().getResultadoFinalEquipo2()==null ||
							partido.get().getResultadoFinalEquipo2()<0) {
				log.warn("Piden resultados de un partido que no ha empezado!"+idPartido);
				return new ArrayList<PrediccionesUsuario>();
			}
			List<PrediccionesUsuario> prediccionesDto = new ArrayList<PrediccionesUsuario>();
			List<PrediccionesUsuario> predicciones = this.prediccionesUsuarioDAO.findByPolla(polla.get());
			for (PrediccionesUsuario prediccion : predicciones) {
				List<PrediccionPartido> prediccionPartido = prediccion.getPrediccionPartido().stream().filter(x -> x.getPartido().getIdPartido().equals(idPartido)).collect(Collectors.toList());
				PrediccionesUsuario dto = new PrediccionesUsuario();
				dto.setUsuario(prediccion.getUsuario());
				dto.setPrediccionPartido(prediccionPartido);
				prediccionesDto.add(dto);
			}
			
			return prediccionesDto;
		}
		
		return new ArrayList<PrediccionesUsuario>();
		
	}

}
