package com.pollapp.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollapp.dao.DeporteDAO;
import com.pollapp.model.Deporte;
import com.pollapp.service.DeporteService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class DeporteServiceImpl implements DeporteService {

	@Autowired
	private DeporteDAO deporteDAO;
	
	@Override
	public Deporte saveDeporte(Deporte deporte) {
		log.info("Guardar deporte");
		return deporteDAO.save(deporte);
	}

	@Override
	public List<Deporte> getAllDeporte() {
		log.info("Obtener todos los deportes");
		Iterable<Deporte> iterable = deporteDAO.findAll();
		
		return StreamSupport.stream(iterable.spliterator(), false) 
	            .collect(Collectors.toList()); 
	}

	@Override
	public Deporte getDeporte(int idDeporte) {
		log.info("Obtener deporte");
		return deporteDAO.findById(idDeporte).orElse(null);
	}

	@Override
	public void deleteDeporte(int idDeporte) {
		log.info("Eliminar deporte");
		deporteDAO.deleteById(idDeporte);
		
	}

}
