package com.pollapp.service;

public interface MailService {
	public void sendEmail(String recipient, String subject, String body);
	public void sendEmail(String[] recipients, String subject, String body);
}
