package com.pollapp.config;


import java.util.concurrent.Executor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.pollapp.config.jwt.JwtAuthenticationEntryPoint;
import com.pollapp.config.jwt.JwtAuthenticationFilter;
import com.pollapp.service.impl.MyUserDetailsService;

@Configuration
@ConfigurationProperties(prefix = "app")
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)si sse quiere que haya seguridad a nivel de metodo dependiendo del rol
public class AppConfiguration 
  extends WebSecurityConfigurerAdapter {
	
	private String equipoUpdaterScheduledCron;
	private String partidoUpdaterScheduledCron;
	private String resultadosUpdaterScheduledCron;
	private String applicationKey;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
  	private JwtAuthenticationEntryPoint unauthorizedHandler;
 
    @Override
    protected void configure(AuthenticationManagerBuilder auth)
      throws Exception {
        /*auth
          .inMemoryAuthentication()
          .withUser("user")
          .password("{noop}password")
          .roles("USER");*/
    	auth.authenticationProvider(authenticationProvider());
    }
    
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }
     
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }
    
   
    
    @Bean
    public UserDetailsService userDetailsService() {
        return new MyUserDetailsService();
    }
    
    @Bean
    public JwtAuthenticationFilter authenticationTokenFilterBean() throws Exception {
        return new JwtAuthenticationFilter();
    }
    
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
 
    @Override
    protected void configure(HttpSecurity http) 
      throws Exception {
    	
        http.csrf().disable()
          .authorizeRequests()
          .antMatchers("/h2-console", "/h2-console/*","/*.js","/*.css", "/fontawesome*", "/assets/i18n/*.*", "/assets/*.*", "/favicon.ico", "/assets/images/*.*", "/","/login","/sso/*","/#/sso/*", "/api/security/validateTicket/*", "/api/security/activateAccount/*", "/activationSuccess","/activationError", "/api/security/sso/*", "/api/security/signon", "/api/security/register", "/api/security/resetPassword", "/api/comentario/*").permitAll()
          .anyRequest().authenticated()
          .and()
          .formLogin()
          .loginPage("/")//login?
          .loginProcessingUrl("/api/security/login")
          .and().exceptionHandling()
          .authenticationEntryPoint(unauthorizedHandler)
          .and()
          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        
        http
        	.headers().frameOptions().disable()
        	.and()
        	.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
          //.addFilter(new JWTAuthenticationFilter(authenticationManager()))
          //.addFilter(new JWTAuthorizationFilter(authenticationManager()))
          // this disables session creation on Spring Security

          //.httpBasic();
    
    }
    
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
    
    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("Stackoverflow-");
        executor.initialize();
        return executor;
    }

	public String getApplicationKey() {
		return applicationKey;
	}

	public void setApplicationKey(String applicationKey) {
		this.applicationKey = applicationKey;
	}

	public String getEquipoUpdaterScheduledCron() {
		return equipoUpdaterScheduledCron;
	}

	public void setEquipoUpdaterScheduledCron(String equipoUpdaterScheduledCron) {
		this.equipoUpdaterScheduledCron = equipoUpdaterScheduledCron;
	}

	public String getPartidoUpdaterScheduledCron() {
		return partidoUpdaterScheduledCron;
	}

	public void setPartidoUpdaterScheduledCron(String partidoUpdaterScheduledCron) {
		this.partidoUpdaterScheduledCron = partidoUpdaterScheduledCron;
	}

	public String getResultadosUpdaterScheduledCron() {
		return resultadosUpdaterScheduledCron;
	}

	public void setResultadosUpdaterScheduledCron(String resultadosUpdaterScheduledCron) {
		this.resultadosUpdaterScheduledCron = resultadosUpdaterScheduledCron;
	}
    
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/notFound").setViewName("forward:/index.html");
//    }
//
//
//    @Bean
//    public EmbeddedServletContainerCustomizer containerCustomizer() {
//        return container -> {
//            container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND,
//                    "/notFound"));
//        };
//    }
    
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/").setViewName("forward:/index.html");
//    }
    
    /*@Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        //mailSender.setHost("localhost");
        //mailSender.setPort(2500);
         
        //mailSender.setUsername("my.gmail@gmail.com");
        //mailSender.setPassword("password");
         
        //Properties props = mailSender.getJavaMailProperties();
        //props.put("mail.transport.protocol", "smtp");
        //props.put("mail.smtp.auth", "false");
        //props.put("mail.smtp.starttls.enable", "true");
        //props.put("mail.debug", "true");
         
        return mailSender;
    }*/
}
