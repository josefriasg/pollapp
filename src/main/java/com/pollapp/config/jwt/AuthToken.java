package com.pollapp.config.jwt;

public class AuthToken {

    private String token;
    private String username;
    private String name;
    private int role;

    public AuthToken(){

    }

    public AuthToken(String token, String username, String name, int role){
        this.token = token;
        this.username = username;
        this.name = name;
        this.role = role;
    }

    public AuthToken(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}
    
    
}
