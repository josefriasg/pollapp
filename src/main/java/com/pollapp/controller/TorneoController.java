package com.pollapp.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollapp.model.Torneo;
import com.pollapp.service.TorneoService;
import com.pollapp.service.impl.UsuarioServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/torneo")
public class TorneoController {
	
	@Autowired
	private TorneoService torneoService;
	
	@RequestMapping(value = "/list")
	public List<Torneo> getAll(){
		log.info("Listar torneos");
		return torneoService.getAllTorneo();
	}
	
	@RequestMapping(value = "/list/active")
	public List<Torneo> getAllActive(){
		log.info("Listar torneos activos");
		return torneoService.getAllTorneoActive();
	}
	
	@RequestMapping(value = "/save")
	public Torneo saveTorneo(@RequestBody Torneo torneo) {
		log.info("Guardar torneo");
		return torneoService.saveTorneo(torneo);
	}
	
	@RequestMapping(value = "/get/{idTorneo}")
	public Torneo getTorneoById(@PathVariable("idTorneo") Integer idTorneo) throws IOException {
		log.info("Obtener torneo");
        Torneo categoria= torneoService.getTorneo(idTorneo.intValue());
        return categoria;
	}
	
	@RequestMapping(value = "/delete/{idTorneo}")
	public void deleteTorneo(@PathVariable("idTorneo") Integer idTorneo) {
		log.info("Borrar torneo");
		torneoService.deleteTorneo(idTorneo);
		
	}

}
