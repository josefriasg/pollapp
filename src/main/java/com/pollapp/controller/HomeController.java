package com.pollapp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class HomeController {

	
	@RequestMapping(value = "/login")
	public RedirectView redirectToHome(
		      RedirectAttributes attributes) {
		        //attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
		        //attributes.addAttribute("attribute", "redirectWithRedirectView");
		        return new RedirectView("/");
		    }
}
