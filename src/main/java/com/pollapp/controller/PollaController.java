package com.pollapp.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollapp.dto.EventoCalendario;
import com.pollapp.dto.PartidoDto;
import com.pollapp.dto.PollaBasicDto;
import com.pollapp.dto.PollaDto;
import com.pollapp.dto.PosicionDto;
import com.pollapp.dto.PrediccionPartidoDto;
import com.pollapp.dto.PrediccionesUsuarioDto;
import com.pollapp.dto.ResultadoOperacionDto;
import com.pollapp.model.Partido;
import com.pollapp.model.Polla;
import com.pollapp.model.PrediccionPartido;
import com.pollapp.model.PrediccionesUsuario;
import com.pollapp.model.Usuario;
import com.pollapp.service.PollaService;
import com.pollapp.service.UsuarioService;
import com.pollapp.service.impl.UsuarioServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/polla")
public class PollaController {
	
	@Autowired
	private PollaService pollaService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@RequestMapping(value = "/list")
	public List<PollaDto> getAll(Principal principal){
		log.info("Listar pollas");
		String name = principal.getName();
		Usuario loggedUser = usuarioService.getUsuarioByEmail(name);
		List<Polla> pollas= pollaService.getAllPolla(loggedUser);
		List<PollaDto> pollasDto = pollas.stream().map(x->modelMapper.map(x, PollaDto.class)).collect(Collectors.toList());
		pollasDto.stream().forEach(x->x.setPrediccionesUsuario(null));
		return pollasDto;
	}
	
	@RequestMapping(value = "/save")
	public int savePolla(@RequestBody PollaDto pollaDto, Principal principal) {
		log.info("Guardar polla");
		Polla polla = modelMapper.map(pollaDto, Polla.class);
		try {
			String name = principal.getName();
			Usuario loggedUser = usuarioService.getUsuarioByEmail(name);
			polla.setCreador(loggedUser);
			Polla saved = pollaService.savePolla(polla);
			return saved.getIdPolla();
		} catch (Exception e) {
			log.error("No se pudo guardar la polla", e);
			return -2;
		}
		
	}
	
	@RequestMapping(value = "/get/{idPolla}")
	public PollaDto getPollaById(@PathVariable("idPolla") Integer idPolla) throws IOException {
		log.info("Obtener polla");
        Polla polla= pollaService.getPolla(idPolla.intValue());
        return modelMapper.map(polla, PollaDto.class);
	}
	
	@RequestMapping(value = "/getBasic/{idPolla}")
	public PollaBasicDto getPollaBasicById(@PathVariable("idPolla") Integer idPolla) throws IOException {
		log.info("Obtener informacion basica de la polla");
        Polla polla= pollaService.getPolla(idPolla.intValue());
        return modelMapper.map(polla, PollaBasicDto.class);
	}
	
	@RequestMapping(value = "/getPollaUniqueCode/{idPolla}")
	public String getPollaUniqueCode(@PathVariable("idPolla") Integer idPolla) throws IOException {
		log.info("Obtener codigo unico de polla");
        Polla polla= pollaService.getPolla(idPolla.intValue());
        return polla.getUniqueCode();
	}
	
	
	@RequestMapping(value = "/delete/{idPolla}")
	public void deletePolla(@PathVariable("idPolla") Integer idPolla) {
		log.info("Borrar polla");
		pollaService.deletePolla(idPolla);
		
	}
	
	@RequestMapping(value = "/join/{codigoPolla}")
	public int joinPolla(@PathVariable("codigoPolla") String codigoPolla, Principal principal) {
		log.info("Unirse a polla");
		String name = principal.getName();
		Usuario loggedUser = usuarioService.getUsuarioByEmail(name);
		return pollaService.addUserToPolla(loggedUser, codigoPolla);
		
	}
	
	@RequestMapping(value = "/posiciones/{idPolla}")
	public List<PosicionDto> getPosiciones(@PathVariable("idPolla") Integer idPolla){
		log.info("Obtener posiciones de polla");
		Polla polla= pollaService.getPolla(idPolla.intValue());
		return this.calculatePosiciones(polla, false);
	}
	
	@RequestMapping(value = "/posiciones/picture/{idPolla}")
	public List<PosicionDto> getPosicionesPicture(@PathVariable("idPolla") Integer idPolla){
		log.info("Obtener posiciones de polla con imagen");
		Polla polla= pollaService.getPolla(idPolla.intValue());
		return this.calculatePosiciones(polla, true);
	}
	

	private List<PosicionDto> calculatePosiciones(Polla polla, boolean withPicture) {
		log.info("Calcular posiciones");
		List<PosicionDto> posiciones = new ArrayList<PosicionDto>();
		
		for (PrediccionesUsuario predicciones: polla.getPrediccionesUsuario()) {
			PosicionDto posicion=new PosicionDto();
			posicion.setNombre(predicciones.getUsuario().getName());
			posicion.setUserId(predicciones.getUsuario().getUserId());
			if (withPicture) {
				posicion.setPicture(predicciones.getUsuario().getPicture());	
			}
			
			int puntos = 0;
			for (PrediccionPartido prediccion : predicciones.getPrediccionPartido()) {
				puntos += calculatePuntosDiferencia(polla,prediccion);
				puntos += calculatePuntosExactoEquipo(polla,prediccion);
				puntos += calculatePuntosGanador(polla,prediccion);
			}
			posicion.setPuntos(puntos);
			posiciones.add(posicion);
		}
		
		//treeset para ordenado?
		
		posiciones.sort((PosicionDto p1, PosicionDto p2) -> p2.compareTo(p1));
		for (int i = 0; i<posiciones.size(); i++) {
			int numEmpates=this.setRanking(i, i+1, 0, posiciones);
			i+=numEmpates;
		}
		return posiciones;
		
	}

	private int setRanking(int i, int pos, int numEmpates, List<PosicionDto> posiciones) {
		posiciones.get(i).setPosicion(pos);
		if (i+1<posiciones.size() && posiciones.get(i+1).getPuntos()==posiciones.get(i).getPuntos()) {
			numEmpates++;
			numEmpates = this.setRanking(i+1,pos, numEmpates, posiciones);
		}
		return numEmpates;
		
	}

	private int calculatePuntosGanador(Polla polla, PrediccionPartido prediccion) {
		if (prediccion.getPartido().getResultadoFinalEquipo1()==null || prediccion.getPartido().getResultadoFinalEquipo2()==null || 
				prediccion.getPartido().getResultadoFinalEquipo1()==-1 || prediccion.getPartido().getResultadoFinalEquipo2()==-1 ||
				prediccion.getResultadoEquipo1() == null || prediccion.getResultadoEquipo2() == null) {
			return 0;
		}
		
		int puntos = 0;
		
		int resultado = 0;
		
		if (prediccion.getPartido().getResultadoFinalEquipo1() == prediccion.getPartido().getResultadoFinalEquipo2()) {
			resultado = 1;
		}else if (prediccion.getPartido().getResultadoFinalEquipo1() > prediccion.getPartido().getResultadoFinalEquipo2()) {
			resultado = 2;
		}else if (prediccion.getPartido().getResultadoFinalEquipo1() < prediccion.getPartido().getResultadoFinalEquipo2()) {
			resultado = 3;
		}
		
		
		int predic = -1;
		
		if (prediccion.getResultadoEquipo1() == prediccion.getResultadoEquipo2()) {
			predic = 1;
		}else if (prediccion.getResultadoEquipo1() > prediccion.getResultadoEquipo2()) {
			predic = 2;
		}else if (prediccion.getResultadoEquipo1() < prediccion.getResultadoEquipo2()) {
			predic = 3;
		}
		
		puntos = resultado == predic?polla.getPuntosGanadorOEmpate():0;
		return puntos;
	}

	private int calculatePuntosExactoEquipo(Polla polla, PrediccionPartido prediccion) {
		if (prediccion.getPartido().getResultadoFinalEquipo1()==null || prediccion.getPartido().getResultadoFinalEquipo2()==null || 
				prediccion.getPartido().getResultadoFinalEquipo1()==-1 || prediccion.getPartido().getResultadoFinalEquipo2()==-1 ||
				prediccion.getResultadoEquipo1() == null || prediccion.getResultadoEquipo2() == null) {
			return 0;
		}
		
		int puntos = 0;
		if (prediccion.getPartido().getResultadoFinalEquipo1() == prediccion.getResultadoEquipo1()) {
			puntos += polla.getPuntosExactoEquipo();
		}
		
		if (prediccion.getPartido().getResultadoFinalEquipo2() == prediccion.getResultadoEquipo2()) {
			puntos += polla.getPuntosExactoEquipo();
		}
		
		return puntos;
		
	}

	private int calculatePuntosDiferencia(Polla polla, PrediccionPartido prediccion) {
		if (prediccion.getPartido().getResultadoFinalEquipo1()==null || prediccion.getPartido().getResultadoFinalEquipo2()==null || 
				prediccion.getPartido().getResultadoFinalEquipo1()==-1 || prediccion.getPartido().getResultadoFinalEquipo2()==-1 ||
				prediccion.getResultadoEquipo1() == null || prediccion.getResultadoEquipo2() == null) {
			return 0;
		}
		int difReal = prediccion.getPartido().getResultadoFinalEquipo1() - prediccion.getPartido().getResultadoFinalEquipo2();
		int difPred = prediccion.getResultadoEquipo1() - prediccion.getResultadoEquipo2();
		
		return difReal == difPred? polla.getPuntosDiferencia():0;
	}
	
	@RequestMapping(value = "/predicciones/{action}/{idPolla}")
	public List<PrediccionPartidoDto> getPredicciones(@PathVariable("idPolla") Integer idPolla,@PathVariable("action") String action, Principal principal){
		log.info("Obtener prediccion:"+action);
		String name = principal.getName();
		Usuario loggedUser = usuarioService.getUsuarioByEmail(name);
		List<PrediccionPartido> prediccionPartido = this.pollaService.getPrediccionesPolla(idPolla,action, loggedUser);
		List<PrediccionPartidoDto> prediccionesDto = prediccionPartido
				  .stream()
				  .map(prediccion -> modelMapper.map(prediccion, PrediccionPartidoDto.class))
				  .collect(Collectors.toList());
		return prediccionesDto;
	}
	
	@RequestMapping(value = "/predicciones/all/{viewDate}")
	public List<EventoCalendario> getAllPredicciones(@PathVariable("viewDate") Date viewDate, Principal principal){
		log.info("Obtener predicciones por fecha:"+viewDate);
		String name = principal.getName();
		Usuario loggedUser = usuarioService.getUsuarioByEmail(name);
		List<EventoCalendario> prediccionPartido = this.pollaService.getAllPrediccionesMonth(loggedUser, viewDate);
		return prediccionPartido;
	}
	
	@RequestMapping(value = "/predicciones/user/{idPolla}/{idUser}")
	public List<PrediccionPartidoDto> getPredicciones(@PathVariable("idPolla") Integer idPolla, @PathVariable("idUser") Integer idUser){
		log.info("Obtener predicciones de usuario y polla");
		Usuario user = usuarioService.getUsuario(idUser);
		List<PrediccionPartido> prediccionPartido = this.pollaService.getPrediccionesPolla(idPolla, user);
		
		//quitar los partidos que no han empezado
		prediccionPartido.removeIf(x -> x.getPartido().getResultadoFinalEquipo1()==null || 
				x.getPartido().getResultadoFinalEquipo1()<0 || 
				x.getPartido().getResultadoFinalEquipo2()==null ||
				x.getPartido().getResultadoFinalEquipo2()<0||
				x.getResultadoEquipo1()==null||
				x.getResultadoEquipo2()==null||
				x.getResultadoEquipo1()<0||
				x.getResultadoEquipo2()<0);
		
		List<PrediccionPartidoDto> prediccionesDto = prediccionPartido
				  .stream()
				  .map(prediccion -> modelMapper.map(prediccion, PrediccionPartidoDto.class))
				  .collect(Collectors.toList());
		return prediccionesDto;
	}

	@RequestMapping(value = "/save/predicciones/{idPolla}")
	public ResultadoOperacionDto savePredicciones(@PathVariable("idPolla") Integer idPolla, @RequestBody List<PrediccionPartidoDto> prediccionesDto, Principal principal){
		log.info("Guardar predicciones");
		String name = principal.getName();
		Usuario loggedUser = usuarioService.getUsuarioByEmail(name);
		try {
			List<PrediccionPartido> predicciones = prediccionesDto
					  .stream()
					  .map(prediccionDto -> modelMapper.map(prediccionDto, PrediccionPartido.class))
					  .collect(Collectors.toList());
			int result = this.pollaService.savePredicciones(idPolla, predicciones, loggedUser);
			if (result == 0) {
				return new ResultadoOperacionDto(result, "Predicciones guardadas");
			}else {
				return new ResultadoOperacionDto(result, "Algunas predicciones no se pudieron guardar por inconsistencias");
			}
		}catch (Exception e) {
			e.printStackTrace();
			return new ResultadoOperacionDto(-2, "Error guardando predicciones");
		}
	}
	
	@RequestMapping(value = "/count")
	public Integer getCountPollas(Principal principal){
		log.info("Obtener conteo");
		String name = principal.getName();
		Usuario loggedUser = usuarioService.getUsuarioByEmail(name);
		List<Polla> pollas= pollaService.getAllPolla(loggedUser);
		return pollas.size();
	}
	
	@RequestMapping(value = "/partidos/{idPolla}")
	public List<PartidoDto> getPartidosEmpezados(@PathVariable("idPolla") Integer idPolla, Principal principal){
		List<Partido> partidos = pollaService.getPartidosEmpezadosFromPolla(idPolla);
		
		List<PartidoDto> partidosDto = partidos
				  .stream()
				  .map(partido -> modelMapper.map(partido, PartidoDto.class))
				  .collect(Collectors.toList());
		
		return partidosDto;
	}
	
	@RequestMapping(value = "/predicciones/partido/polla/{idPartido}/{idPolla}")
	public List<PrediccionesUsuarioDto> getPrediccionesPartidoPolla(@PathVariable("idPolla") Integer idPolla, @PathVariable("idPartido") Integer idPartido){
		List<PrediccionesUsuario> predicciones = pollaService.getPrediccionesPartidoPolla(idPolla, idPartido);
		
		List<PrediccionesUsuarioDto> prediccionesDto = predicciones
				  .stream()
				  .map(prediccion -> modelMapper.map(prediccion, PrediccionesUsuarioDto.class))
				  .collect(Collectors.toList());
		
		prediccionesDto.sort((PrediccionesUsuarioDto pred1, PrediccionesUsuarioDto pred2) -> pred1.getUsuario().getName().compareTo(pred2.getUsuario().getName()));
		
		return prediccionesDto;
	}
	
	

}
