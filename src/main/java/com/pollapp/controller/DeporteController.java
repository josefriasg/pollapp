package com.pollapp.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollapp.model.Deporte;
import com.pollapp.service.DeporteService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/deporte")
public class DeporteController {
	
	@Autowired
	private DeporteService deporteService;
	
	@RequestMapping(value = "/list")
	public List<Deporte> getAll(){
		log.info("Listar deporte");
		return deporteService.getAllDeporte();
	}
	
	@RequestMapping(value = "/save")
	public Deporte saveDeporte(@RequestBody Deporte deporte) {
		log.info("Guardar deporte");
		return deporteService.saveDeporte(deporte);
	}
	
	@RequestMapping(value = "/get/{idDeporte}")
	public Deporte getDeporteById(@PathVariable("idDeporte") Integer idDeporte) throws IOException {
		log.info("Obtener deporte");
        Deporte categoria= deporteService.getDeporte(idDeporte.intValue());
        return categoria;
	}
	
	@RequestMapping(value = "/delete/{idDeporte}")
	public void deleteDeporte(@PathVariable("idDeporte") Integer idDeporte) {
		log.info("Borrar deporte");
		deporteService.deleteDeporte(idDeporte);
		
	}

}
