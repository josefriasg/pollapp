package com.pollapp.controller;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Size;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollapp.dto.ComentarioDto;
import com.pollapp.model.Comentario;
import com.pollapp.model.Usuario;
import com.pollapp.service.ComentarioService;
import com.pollapp.service.UsuarioService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/comentario")
public class ComentarioController {

	@Autowired
	private ComentarioService comentarioService;
	

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@RequestMapping(value = "/list")
	public List<ComentarioDto> getAll(){
		log.info("Listar comentarios");
		List<Comentario> comentarios = comentarioService.getAllComentario();
		List<ComentarioDto> comentariosDto = comentarios.stream().map(x->modelMapper.map(x, ComentarioDto.class)).collect(Collectors.toList());
		return comentariosDto;
	}
	
	@RequestMapping(value = "/save")
	public int saveComentario(@RequestBody String comentario, Principal principal) {
		log.info("Guardar comentario");
		if (comentario.length()>255){
			return -1;
		}
		Usuario loggedUser = null;
		if (principal != null) {
			String name = principal.getName();
			loggedUser = usuarioService.getUsuarioByEmail(name);
		}
		if (comentarioService.saveComentario(comentario, loggedUser) !=null) {
			return 0;
		}else {
			return -1;
		}
	}
}
