package com.pollapp.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollapp.model.Pais;
import com.pollapp.service.PaisService;
import com.pollapp.service.impl.UsuarioServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/pais")
public class PaisController {
	
	@Autowired
	private PaisService paisService;
	
	@RequestMapping(value = "/list")
	public List<Pais> getAll(){
		log.info("Listar paises");
		return paisService.getAllPais();
	}
	
	@RequestMapping(value = "/save")
	public Pais savePais(@RequestBody Pais pais) {
		log.info("Guardar paises");
		return paisService.savePais(pais);
	}
	
	@RequestMapping(value = "/get/{idPais}")
	public Pais getPaisById(@PathVariable("idPais") Integer idPais) throws IOException {
		log.info("Obtener pais");
        Pais categoria= paisService.getPais(idPais.intValue());
        return categoria;
	}
	
	@RequestMapping(value = "/delete/{idPais}")
	public void deletePais(@PathVariable("idPais") Integer idPais) {
		log.info("Borrar pais");
		paisService.deletePais(idPais);
		
	}

}
