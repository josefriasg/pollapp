package com.pollapp.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr; 
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pollapp.dto.PollaDto;
import com.pollapp.dto.UsuarioDto;
import com.pollapp.dto.UsuarioPictureDto;
import com.pollapp.model.Usuario;
import com.pollapp.service.UsuarioService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	private static int FILE_MAX_SIZE=2097152;//2MB
	
	@RequestMapping(value = "/list")
	public List<Usuario> getAll(){
		log.info("Listar usuarios");
		return usuarioService.getAllUsuario();
	}
	
	@RequestMapping(value = "/save")
	public Usuario saveUsuario(@RequestBody Usuario usuario) {
		log.info("Guardando usuario");
		return usuarioService.saveUsuario(usuario);
	}
	
	@RequestMapping(value = "/update/notification/{notification}")
	public boolean updateNotification(@PathVariable("notification") boolean notification, Principal principal) {
		log.info("Actualizando notificacion");
		Usuario user = usuarioService.getUsuarioByEmail(principal.getName());
		user.setWantEmails(notification);
		Usuario saved = usuarioService.saveUsuario(user);
		if (saved!=null) {
			return true;
		}else {
			return false;
		}
	}
	
	@RequestMapping(value = "/get/notification")
	public boolean getNotification(Principal principal) {
		log.info("Obteniendo notificacion");
		Usuario user = usuarioService.getUsuarioByEmail(principal.getName());
		return user.isWantEmails();
	}
	
	@RequestMapping(value = "/get/{idUsuario}")
	public UsuarioDto getUsuarioById(@PathVariable("idUsuario") Integer idUsuario) throws IOException {
		log.info("Obteniendo usuario por Id");
        Usuario usuario= usuarioService.getUsuario(idUsuario.intValue());
        UsuarioDto retUser=modelMapper.map(usuario, UsuarioDto.class);
        //retUser.setPicture(this.decompressBytes(usuario.getPicture()));
        return retUser;
	}
	
	@RequestMapping(value = "/delete/{idUsuario}")
	public void deleteUsuario(@PathVariable("idUsuario") Integer idUsuario) {
		log.info("Borrando usuario");
		usuarioService.deleteUsuario(idUsuario);
	}
	
	@RequestMapping(value = "/download/picture/{username}")
	public UsuarioPictureDto getProfilePicture(@PathVariable("username") String username) throws IOException {
		log.info("Obteniendo imagen usuario");
        Usuario usuario= usuarioService.getUsuarioByEmail(username.toLowerCase());
    	UsuarioPictureDto retUser=modelMapper.map(usuario, UsuarioPictureDto.class);
//    	if (usuario.getPicture()!=null) {
//    		retUser.setPicture(this.decompressBytes(usuario.getPicture()));
//    	}
    	
    	return retUser;    
	}
	
	@RequestMapping(value = "/download/picture")
	public UsuarioPictureDto getProfilePicture(Principal principal) throws IOException {
		return this.getProfilePicture(principal.getName());  
	}
	
	
	
	@PostMapping("/upload/picture")
	public int uplaodPicture(@RequestParam("imageFile") MultipartFile file, Principal principal) {
        try {
        	log.info("Original Image Byte Size - " + file.getBytes().length);
			if (file.getBytes().length > UsuarioController.FILE_MAX_SIZE) {
				log.warn("Archivo muy grande");
				return 1;
			}
			
			if (!(file.getOriginalFilename().toLowerCase().endsWith(".bmp")||file.getOriginalFilename().toLowerCase().endsWith(".jpg")||file.getOriginalFilename().toLowerCase().endsWith(".jpeg")||file.getOriginalFilename().toLowerCase().endsWith(".png"))) {
				log.warn("Error en formato de archivo");
				return 2;
			}
			Usuario user = usuarioService.getUsuarioByEmail(principal.getName());
			
			return usuarioService.uploadPicture(user, file);
		} catch (IOException e) {
			log.error("Error con el archivo", e);
			return 3;
		}
        
    }
	
	
	
}
