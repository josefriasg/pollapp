package com.pollapp.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollapp.dto.PartidoDto;
import com.pollapp.dto.ResultadoOperacionDto;
import com.pollapp.model.Partido;
import com.pollapp.service.PartidoService;
import com.pollapp.service.impl.UsuarioServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/partido")
public class PartidoController {
	
	@Autowired
	private PartidoService partidoService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@RequestMapping(value = "/list")
	public List<Partido> getAll(){
		log.info("Listar partidos");
		return partidoService.getAllPartido();
	}
	
	@RequestMapping(value = "/list/{idTorneo}")
	public List<PartidoDto> getAllFromTorneo(@PathVariable("idTorneo") Integer idTorneo){
		log.info("Listar partidos de un torneo");
		
		List<PartidoDto> partidosDto = partidoService.getAllPartidoForTorneo(idTorneo).stream()
				  .map(partidos -> modelMapper.map(partidos, PartidoDto.class))
				  .collect(Collectors.toList());
		
		List<PartidoDto> output = partidosDto.stream().map(partido -> this.checkResetMarcador(partido)).collect(Collectors.toList());
		return output;
	}
	
	private PartidoDto checkResetMarcador(PartidoDto partido) {
		if (partido.getResultadoFinalEquipo1()==null || partido.getResultadoFinalEquipo1()==-1) {
			log.info("Reseteando marcador equipo 1");
			partido.setResultadoFinalEquipo1(null);
		}
		
		if (partido.getResultadoFinalEquipo2()==null || partido.getResultadoFinalEquipo2()==-1) {
			log.info("Reseteando marcador equipo 2");
			partido.setResultadoFinalEquipo2(null);
		}
		
		return partido;
	}

	@RequestMapping(value = "/save")
	public Partido savePartido(@RequestBody Partido partido) {
		log.info("Guardar partido");
		return partidoService.savePartido(partido);
	}
	
	@RequestMapping(value = "/save/marcadores")
	public ResultadoOperacionDto saveMarcadores(@RequestBody Partido[] partidos) {
		log.info("Guardar marcadores");
		return partidoService.saveMarcadores(partidos);
	}
	
	@RequestMapping(value = "/get/{idPartido}")
	public Partido getPartidoById(@PathVariable("idPartido") Integer idPartido) throws IOException {
		log.info("Obtener partido");
        Partido categoria= partidoService.getPartido(idPartido.intValue());
        return categoria;
	}
	
	@RequestMapping(value = "/delete/{idPartido}")
	public void deletePartido(@PathVariable("idPartido") Integer idPartido) {
		log.info("Borrar partido");
		partidoService.deletePartido(idPartido);
	}

}
