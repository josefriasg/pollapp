package com.pollapp.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollapp.model.Equipo;
import com.pollapp.service.EquipoService;
import com.pollapp.service.impl.UsuarioServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/equipo")
public class EquipoController {
	
	@Autowired
	private EquipoService equipoService;
	
	@RequestMapping(value = "/list")
	public List<Equipo> getAll(){
		log.info("Listar equipos");
		return equipoService.getAllEquipo();
	}
	
	@RequestMapping(value = "/list/{idPais}")
	public List<Equipo> getAllFromPais(@PathVariable("idPais") Integer idPais){
		log.info("Listar equipos de pais");
		return equipoService.getAllEquipoForPais(idPais);
	}
	
	@RequestMapping(value = "/save")
	public Equipo saveEquipo(@RequestBody Equipo equipo) {
		log.info("Guardar equipo");
		return equipoService.saveEquipo(equipo);
	}
	
	@RequestMapping(value = "/get/{idEquipo}")
	public Equipo getEquipoById(@PathVariable("idEquipo") Integer idEquipo) throws IOException {
		log.info("Obtener equipo");
        Equipo categoria= equipoService.getEquipo(idEquipo.intValue());
        return categoria;
	}
	
	@RequestMapping(value = "/delete/{idEquipo}")
	public void deleteEquipo(@PathVariable("idEquipo") Integer idEquipo) {
		log.info("Borrar equipo");
		equipoService.deleteEquipo(idEquipo);
		
	}

}
