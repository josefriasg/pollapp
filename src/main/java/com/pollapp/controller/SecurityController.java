package com.pollapp.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.LinkedHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.pollapp.config.jwt.AuthToken;
import com.pollapp.config.jwt.JwtTokenUtil;
import com.pollapp.model.Usuario;
import com.pollapp.model.enums.Rol;
import com.pollapp.service.UsuarioService;
import com.pollapp.service.impl.UsuarioServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/security")
public class SecurityController {
	
	@Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	@Autowired
	private UsuarioService usuarioService;
	
	
	@RequestMapping(value = "/signon")
	//public boolean login(@RequestBody Usuario user, Authentication auth) throws IOException {
		public ResponseEntity<AuthToken> login(@RequestBody Usuario loginUser) throws IOException {
		if (loginUser != null) {
			log.info("Logueando al usuario:"+loginUser.getEmail());
			loginUser.setEmail(loginUser.getEmail().toLowerCase());
		}
		
		try {
			final Authentication authentication = authenticationManager.authenticate(
			        new UsernamePasswordAuthenticationToken(
			        		loginUser.getEmail(),
			        		loginUser.getPassword()
			        )
			);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			final Usuario user = usuarioService.getUsuarioByEmail(loginUser.getEmail());
			final String token = jwtTokenUtil.generateToken(user);
			log.info("Logueado exitosamente");
			return ResponseEntity.ok(new AuthToken(token, user.getEmail(), user.getName(), user.getRole().ordinal()));
		} catch (AuthenticationException e) {
			log.warn("Error autenticando al usuario");
			return ResponseEntity.ok(new AuthToken());
		}
        //return ResponseEntity.ok(new AuthToken(token));//https://www.devglan.com/spring-security/spring-boot-jwt-auth
        
        /*authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getEmail(), loginUser.getPassword()));
        final Usuario user = usuarioService.getUsuarioByEmail(loginUser.getEmail());
        final String token = jwtTokenUtil.generateToken(user);
        return new ApiResponse<>(200, "success",new AuthToken(token, user.getUsername()));
		*/
        
        
		/*if (auth != null) {
			auth.setAuthenticated(false);	
		}
		
		Usuario userDB = usuarioService.getUsuarioByEmail(user.getEmail());
		return userDB!=null && !userDB.isDropped() && userDB.isEnabled() && passwordEncoder.matches(user.getPassword(), userDB.getPassword());*/
	}
	
	@RequestMapping(value = "/register")
	public boolean register(@RequestBody Usuario user) throws IOException {
		Usuario userDB = usuarioService.getUsuarioByEmail(user.getEmail());
		if (userDB == null) {
			if (user.getName() != null && !user.getName().equals("")) {
				user.setDropped(false);
				user.setEnabled(true);
				//user.setPassword(passwordEncoder.encode(user.getPassword()));
				user.setRole(Rol.USER);
		        usuarioService.saveUsuario(user);
		        //usuarioService.sendRegistrationEmail(user);
		        log.info("Registrando al usuario:"+user.getEmail());
		        return true;
			}
		}
		return false;
	}
	
	@RequestMapping(value = "/activateAccount/{accountData}")
	public RedirectView activateAccount(@PathVariable("accountData") String accountDataEncoded) throws IOException {
		try {
			String accountData = new String(Base64.getUrlDecoder().decode(accountDataEncoded));
			
			/*	1) ver si existe como usuario
			1.1) si no existe, error 
			1.2) si existe usuario, enabled = true
			1.2.1) enabled = true;
			1.2.2) redirigir a pagaina de cuenta activa exitosamente y link a login
			 */
			
			int usuarioId = Integer.parseInt(accountData);
			Usuario user = usuarioService.getUsuario(usuarioId);
			if (user == null) {
				return new RedirectView("/#/activationError");
			}else {
				if (!user.isEnabled()) {
					user.setEnabled(true);
					usuarioService.saveUsuario(user);
				}
				return new RedirectView("/#/activationSuccess");
			}
		}catch (Exception e) {
			log.error("Error activating account", e);
			return new RedirectView("/#/activationError");
		}
	}
	
	
	@RequestMapping(value = "/sso/{ssoData}")
	public ResponseEntity<AuthToken> performSinglesignon(@PathVariable("ssoData") String ssoDataEncoded) throws IOException {
		String ssoData = new String(Base64.getUrlDecoder().decode(ssoDataEncoded));
		String email=ssoData.substring(6, ssoData.indexOf("&"));

		final Usuario user = usuarioService.getUsuarioByEmail(email);
		
		boolean validated= user!=null && !user.isDropped() && user.isEnabled();
		if (validated) {
			final String token = jwtTokenUtil.generateToken(user);
			return ResponseEntity.ok(new AuthToken(token, user.getEmail(), user.getName(), user.getRole().ordinal()));	
		}else {
			return ResponseEntity.ok(new AuthToken());
		}
		
	}
	
	@RequestMapping(value = "/logout")
	public void logout(Authentication auth) throws IOException {
		log.info("Logout");
		auth.setAuthenticated(false);
	}
	
	@RequestMapping(value = "/changePassword")
	public boolean changePassword(@RequestBody Object params, Principal principal) throws IOException {
		if (params instanceof LinkedHashMap) {
			log.info("Cambiando password");
			LinkedHashMap<String, String> paramsMap = (LinkedHashMap<String, String>)params;
			String currentPassword = paramsMap.get("password");
			
			boolean correctPassword = false;
			
			try {
			Authentication authentication = authenticationManager.authenticate(
			        new UsernamePasswordAuthenticationToken(
			        		principal.getName(),
			        		currentPassword
			        )
			);
			correctPassword = authentication.isAuthenticated();
			
			} catch (Exception e) {
				log.warn("Error en autenticacion", e);	
			}
			
			if (correctPassword) {
				log.info("Password correcto");
				String newPassword = paramsMap.get("newPassword");
				Usuario user = usuarioService.getUsuarioByEmail(principal.getName());
				if (user != null) {
						user.setPassword(passwordEncoder.encode(newPassword));
				        usuarioService.saveUsuario(user);
				        log.info("Password cambiado");
				        return true;
					
				}
			}
		}
		return false;
	}
	
	@RequestMapping(value = "/resetPassword")
	public boolean resetPassword(@RequestBody Object params) throws IOException {
		log.info("Reseteando password");
		if (params instanceof LinkedHashMap) {
			LinkedHashMap<String, String> paramsMap = (LinkedHashMap<String, String>)params;
			String email = paramsMap.get("email");
			log.info("Email enviado a:"+email);
			return usuarioService.resetPassword(email.toLowerCase());
		}
		return false;
	}

}
